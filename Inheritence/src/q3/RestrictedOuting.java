package q3;

public class RestrictedOuting extends Outing
{
	int max;
	
	public RestrictedOuting(int c, int m)
	{
		super(c);
		max = m;
	}
	
	public boolean add()
	{
		if(count>=max) return false;
		else
		{
			count++;
			return true;
		}
	}
	
	public int check_avail_places()
	{
		return max-count;
	}
	
	public int readCount()
	{
		return count;
	}
	
	public void print_details()
	{
		super.print_details();
		
	}

}
