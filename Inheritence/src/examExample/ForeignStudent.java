package examExample;

public class ForeignStudent extends Student
{
	String country_of_origin;
	
	public ForeignStudent(String nm, int age, String coo)
	{
		super(nm,age);
		country_of_origin=coo;
	}
	
	int readAge()
	{
		return age;
	}
	
	String readCountry()
	{
		return country_of_origin;
	}
	
	boolean changeAge(int a)
	{
		if(a>=age)
		{
			age = a;
			return true;
		}
		else return false;
	}
	
	boolean matureStudent()
	{
		if(age>22) return true;
		else return false;
	}
	
	protected void printDetails()
	{
		super.printDetails();
		System.out.println("Country: "+country_of_origin);
	}

}
