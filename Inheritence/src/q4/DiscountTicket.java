package q4;

public class DiscountTicket extends Ticket
{
	int discount;
	
	public DiscountTicket(int p,String s,String e, int d)
	{
		super(p,s,e);
		discount =d;
	}
	
	public void setPrice(int p)
	{
		price = p;
	}
	
	public void setDiscount(int d)
	{
		discount = d;
	}
	
	public int grossPrice()
	{
		return price-discount;
	}

}
