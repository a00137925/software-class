package q1;

public class MyCounter extends Counter
{
	int upper_limit;
	
	public MyCounter(int v, int ul)
	{
		super(v);
		upper_limit =ul;
	}
	
	public boolean increment()
	{
		if(value < upper_limit)
			{
				value++; 
				return true;
			}
		else return false;
	}
	
	int readLimit()
	{
		return upper_limit;
	}

}
