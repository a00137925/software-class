package q2;

public class Cashsave extends Account
{
	int max;
	
	public Cashsave(int n, int b, int m)
	{
		super(n,b);
		max = m;
	}
	
	public boolean deposit(int amt)
	{
		if((amt+balance)<max) 
			{
				balance += amt;
				return true;
			}
		else return false;
	}

}
