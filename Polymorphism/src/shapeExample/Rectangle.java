package shapeExample;

public class Rectangle extends Shape
{
	int height;
	public Rectangle(int l, int h)
	{
		super(l);
		height=h;
	}
	
	public void draw()
	{
		super.draw();
		
		for(int i=0; i<length; i++)
		{			
			for(int j=0; j<height; j++)
			{
				System.out.print("$");
			}
			System.out.println("");
		}
	}

}