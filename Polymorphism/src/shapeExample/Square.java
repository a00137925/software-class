package shapeExample;

public class Square extends Shape
{
	public Square(int l)
	{
		super(l);
	}
	
	public void draw()
	{
		super.draw();
		
		for(int i=0; i<length; i++)
		{			
			for(int j=0; j<length; j++)
			{
				System.out.print("*");
			}
			System.out.println("");
		}
	}

}
