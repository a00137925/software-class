package shapeExample;

public class HorzLine extends Shape
{
	public HorzLine(int l)
	{
		super(l);
	}
	
	public void draw()
	{
		super.draw();
		
		for(int i=0; i<length; i++)
		{
			System.out.print("*");
		}
	}
}
