
public class Limit10 extends Decorator {

	public Limit10(Counter cc) {
		super(cc);
		
	}
	
	public int readValue(){
		return   counter.readValue();
	}
	
	public void increment()
	{
		if(counter.readValue()>9) System.out.println("Limt 10 Reached!");
		else counter.increment();
	}
	
	public void decrement()
	{
		counter.decrement();
	}

}
