
public class UpperLine extends DisplayDecorator
{
	public UpperLine(Display d) 
	{
		super(d);
	}
	
	public void update(String n)
	{
		display.update(n);
	}

	public void print()
	{
		System.out.println("***********************");
		display.print();
		
	}

}
