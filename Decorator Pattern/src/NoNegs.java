
public class NoNegs extends Decorator {

	public NoNegs(Counter cc) 
	{
		super(cc);
	}
	
	public int readValue(){
		return   counter.readValue();
	}
	
	public void decrement()
	{
		if(counter.readValue()<1) System.out.println("No Negative Values!!");
		else counter.decrement();
		
	}
	
	public void increment()
	{
		counter.increment();
	}
	

}
