
public class LowerLine extends DisplayDecorator
{

	public LowerLine(Display d) 
	{
		super(d);
	}
	
	public void update(String n)
	{
		display.update(n);
	}

	public void print()
	{
		display.print();
		System.out.println("***********************");
	}

}
