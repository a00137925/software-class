package name;

import java.rmi.Naming;


public class NameClient
{
	public static void main(String[] args)throws Exception
	{
		//String url =  "rmi:///";
        try 
        {
        	String url= "rmi://192.168.23.46/";      // if object located remotely
        	Name n = (Name) Naming.lookup( url + "toaster" );
        	String d = n.readName();
            System.out.println( "Result: " + d );
            System.in.read();
        } 
        catch( Exception e ) 
        {
        	System.err.println( "Error " + e );
        }

	}

}
