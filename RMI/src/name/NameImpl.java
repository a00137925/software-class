package name;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;



public class NameImpl extends UnicastRemoteObject implements Name
{
	private String first, surname;

	protected NameImpl(String f, String s) throws RemoteException
	{
		first=f;
		surname=s;
	}

	@Override
	public void updateSurname(String s) throws RemoteException
	{
		surname=s;		
	}

	@Override
	public String readName() throws RemoteException
	{
		return first +" "+ surname;
	}

}
