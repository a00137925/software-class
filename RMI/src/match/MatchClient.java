package match;

import java.rmi.Naming;

public class MatchClient
{

	public static void main(String[] args)throws Exception
	{
		String url =  "rmi:///";
        try 
        {
        	//String url= "rmi://193.1.24.1/";      // if object located remotely
        	Match m = (Match) Naming.lookup( url + "toaster" );
        	String d = m.getDescription();
            System.out.println( "Result: " + d );
            System.in.read();
        } 
        catch( Exception e ) 
        {
        	System.err.println( "Error " + e );
        }

	}

}
