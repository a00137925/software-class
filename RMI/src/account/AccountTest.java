package account;
import java.util.Scanner;


class Account
{   private String name;
	private int number;
	private int transactions;
	private int balance;

	public Account(String n,int no)
	{       name=n;
		    number=no;    transactions=0;
		    balance=0;
	}
	public String read_bal()
	{   //transactions++;
		return ""+balance;
	}
	public String read_name()
	{
		return name;
	}
	public int read_number()
	{
		return number;
	}
	public int read_transactions()
		{
			return transactions;
	    }
	public void deposit(int a)
	{   transactions++;
		balance+=a;
	}
	public boolean withdraw(int a)
	{   transactions++;
		balance-=a;
		return  true;}
}

public class AccountTest{
	public static void main(String [] args)
	{
		Account a = new Account("J.Smith",23456);
		int choice = 1;
		Scanner sc = new Scanner(System.in);
		
		while(choice!=5)
		{
         	 System.out.println();
		 System.out.println("1.Deposit");
		 System.out.println("2.Withdraw");
		 System.out.println("3.Check Balance");
		 System.out.println("4.Print Number of Tracsactions");
		 System.out.println("5.Exit");
			System.out.print("\nPlease enter a value-->");
			choice=sc.nextInt();
			if(choice==1)
			{   
			   System.out.print("Enter Amount in Euro: ");
			   int e=sc.nextInt();
				a.deposit(e);
			}
			else if(choice==2)
			{
				   System.out.print("Enter Amount in Euro: ");
				   int e=sc.nextInt();
				boolean res=a.withdraw(e);
				if (res==false){
					System.out.println("Sorry- Insufficient Funds");}
			}
			else if(choice==3)
			{
				System.out.println("Balance="+a.read_bal());}
			else if(choice==4)
			{
				System.out.println("Read Transactions="+a.read_transactions());}
		}  // end while
	}	}