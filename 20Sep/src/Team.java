
public class Team 
{
	private int games_played;
	private int points;
	
	public Team() {};
	public Team(int gp, int p)
	{
		games_played = gp;
		points = p;
	}
	
	void win()
	{
		points+=3;
		games_played++;
	}
	
	void draw()
	{
		points+=1;
		games_played++;

	}
	
	void loss()
	{
		games_played++;

	}
	
	void print_details()
	{
		System.out.println("Games: "+games_played+"\nPoints: "+points);
	}
	
	int read_points()
	{
		return points;
	}
	
	int read_games_played()
	{
		return games_played;
	}
	
	void reset()
	{
		games_played=0;
		points=0;
	}

}
