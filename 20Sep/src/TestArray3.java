class Polls
{
		private int[] values;
		
		public Polls(int [] arr)
		{
				values=arr;
				}

		public int sum()
		{
			int res=0;
			for(int i=0; i<values.length; i++)
			{
				res+=values[i];
			}
         
			return res;
         }
		
		public int highest_figure()
		{   
			int res=0;
			
			for(int i=0; i<values.length; i++)
			{
				if(values[i]>res) res=values[i];
			}
			
			return res;
		}
		
		public boolean greater_than_50()
		{   
			int check=0;
			for(int i=0; i<values.length; i++)
			{
				if(values[i]<=50) check++;
			}
			
			if(check!=0) return false;
			else  return true;
            }
	    
		}




public class TestArray3 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		int[]poll={36,56,59,47,43
				};
		Polls p=new Polls(poll);
		System.out.println("Sum: "+ p.sum());
		System.out.println("Highest Figure: "+ p.highest_figure());
		System.out.println("all Greater Than 50%: "+ p.greater_than_50());
	}

}
