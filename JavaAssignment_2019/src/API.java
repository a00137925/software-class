import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Executable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class API
{
	String apiKey = "17f2008618d3a52925d4dbeca7c6bbd1";
	// BinaryTree theTree = new BinaryTree();
	String genreUrl = "https://api.themoviedb.org/3/genre/movie/list?api_key=17f2008618d3a52925d4dbeca7c6bbd1&language=en-US";
	String stalloneMoviesUrl = "https://api.themoviedb.org/3/person/16483/movie_credits?api_key=17f2008618d3a52925d4dbeca7c6bbd1&language=en-US";
	String schwarzeneggerMoviesUrl = "https://api.themoviedb.org/3/person/1100/movie_credits?api_key=17f2008618d3a52925d4dbeca7c6bbd1&language=en-US";

	BinaryTree stalloneTree = new BinaryTree();
	BinaryTree arnoldTree = new BinaryTree();
	Hashtable<String, Movie> stalloneHT = new Hashtable<String, Movie>();
	Hashtable<String, Movie> arnoldHT = new Hashtable<String, Movie>();

	public static void main(String[] args) throws Exception
	{
		new API();
	}

	public String getJSONFromAPI(String url) throws IOException
	{
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");
		// add request header
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = con.getResponseCode();

		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null)
		{
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}

	public API() throws Exception
	{
		// Read JSON response for the two movie collections and for the list of genres
		String stalloneMovies = getJSONFromAPI(stalloneMoviesUrl);
		String arnoldMovies = getJSONFromAPI(schwarzeneggerMoviesUrl);
		String genreList = getJSONFromAPI(genreUrl);

		getMovies(stalloneMovies, 1);
		getMovies(arnoldMovies, 2);

		// Testing
		System.out.println("\nNode with the key 75 in Stallone collection:");
		System.out.println(stalloneTree.findNode(75));
		System.out.println("\nNode with the key 75 in Arnold collection:");
		System.out.println(arnoldTree.findNode(75).getMovie().getOverview());
		System.out.println("Stallone Hastable Key \"Rambo\": "+stalloneHT.get("Rambo").getTitle());
		//System.out.println(stalloneHT.contains());

	}

	// Find the node with key 75

	public void getMovies(String actor, int x) throws JSONException, IOException
	{
		String genreList = getJSONFromAPI(genreUrl);
		JSONObject JSONobj = new JSONObject(actor);
		JSONObject genreJSONobj = new JSONObject(genreList);

		JSONArray JSONArray = JSONobj.getJSONArray("cast");
		JSONArray genreJSONArray = genreJSONobj.getJSONArray("genres");

		JSONObject jsonObject;
		JSONObject genreJsonObject;
		Movie movie;

		// Loop for creating Stallone movie Nodes and adding them to a BSTree
		for (int i = 0; i < JSONArray.length(); i++)
		{
			jsonObject = JSONArray.getJSONObject(i);
			String title = jsonObject.getString("original_title");
			String character = jsonObject.getString("character");
			String overview = jsonObject.getString("overview");

			String releaseDate = "";
			if (jsonObject.has("release_date"))
				releaseDate = jsonObject.getString("release_date");
			else
				releaseDate = "Not Listed";

			String posterPath = "";
			if (jsonObject.isNull("poster_path"))
				posterPath = "null";
			else
				posterPath = jsonObject.getString("poster_path");

			int rating = jsonObject.getInt("vote_average");

			JSONArray genresArray = jsonObject.getJSONArray("genre_ids");

			String genre = "";
			for (int y = 0; y < genreJSONArray.length(); y++)
			{
				genreJsonObject = genreJSONArray.getJSONObject(y);
				for (int z = 0; z < genresArray.length(); z++)
				{
					if (genreJsonObject.getInt("id") == genresArray.getInt(z))
						genre += genreJsonObject.getString("name") + " ";
				}

			}

			// System.out.println(genre);
			// System.out.println(releaseDate);

			movie = new Movie(i, title, character, overview, genre, releaseDate, posterPath, rating);

			if (x == 1)
			{
				stalloneTree.addNode(i, movie);
				stalloneHT.put(movie.getTitle(), movie);
			}
			else
			{
				arnoldTree.addNode(i, movie);
				arnoldHT.put(movie.getTitle(), movie);
			}
		}

	}

	public BinaryTree getArnoldTree()
	{
		return arnoldTree;
	}

	public BinaryTree getStalloneTree()
	{
		return stalloneTree;
	}
	
	public Hashtable getArnoldHT()
	{
		return arnoldHT;
	}
	
	public Hashtable getStalloneHT()
	{
		return stalloneHT;
	}

	// {
	// "character": "Douglas Quaid/Hauser",
	// "credit_id": "52fe4283c3a36847f8024e9b",
	// "poster_path": "/ikYpJ0AjGBNnAYFnPJDUVIOcduR.jpg",
	// "id": 861,
	// "video": false,
	// "vote_count": 2732,
	// "adult": false,
	// "backdrop_path": "/rPqCxVXBD89jeWMgJU3MeFA6GDV.jpg",
	// "genre_ids": [
	// 28,
	// 12,
	// 878
	// ],
	// "original_language": "en",
	// "original_title": "Total Recall",
	// "popularity": 12.704,
	// "title": "Total Recall",
	// "vote_average": 7.2,
	// "overview": "Construction worker Douglas Quaid discovers a memory chip in his
	// brain during a virtual-reality trip. He also finds that his past has been
	// invented to conceal a plot of planetary domination. Soon, he's off to Mars to
	// find out who he is and who planted the chip.",
	// "release_date": "1990-06-01"
	// },

}
