import java.awt.EventQueue;
import java.awt.Frame;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Window.Type;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Toolkit;

public class ResultsScreen
{

	private JFrame frmResultScreen;
	private JTable table;
	private static ResultsScreen single;
	private static boolean created = false;
	Boolean done = false;
	// private JFrame frmSsSearchScreen;
	JLabel lblNa;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args)
	// {
	// EventQueue.invokeLater(new Runnable()
	// {
	// public void run()
	// {
	// try
	// {
	// ResultsScreen window = new ResultsScreen();
	// window.frmResultScreen.setVisible(true);
	// } catch (Exception e)
	// {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public ResultsScreen(Hashtable HT, BinaryTree Tree, Movie movie) throws IOException
	{
		initialize(HT, Tree, movie);
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize(Hashtable HT, BinaryTree Tree, Movie movie) throws IOException
	{
		frmResultScreen = new JFrame();
		frmResultScreen.setBackground(new Color(245, 245, 245));
		frmResultScreen.getContentPane().setBackground(new Color(245, 245, 245));
		frmResultScreen.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\stallone.png"));
		// frmResultScreen.setType(Type.POPUP);
		frmResultScreen.setTitle("Result Screen");
		frmResultScreen.setBounds(100, 100, 1037, 671);
		frmResultScreen.setLocationRelativeTo(null);
		frmResultScreen.setVisible(true);
		frmResultScreen.addWindowListener(new java.awt.event.WindowAdapter()
		{
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent)
			{
				if (JOptionPane.showConfirmDialog(frmResultScreen, "Are you sure you want to close this window?",
						"Close Window?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
				{
					SearchScreen.bstScore =0;
					SearchScreen.htScore =0;
					created = false;
					SearchScreen.frmSsSearchScreen.setVisible(true);
					frmResultScreen.dispose();
				}
			}
		});
		frmResultScreen.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 52, 1001, 222);
		frmResultScreen.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Title", "Character", "Genre", "Release Date", "Rating"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, String.class, String.class, Integer.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(30);
		table.getColumnModel().getColumn(0).setMaxWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(205);
		table.getColumnModel().getColumn(2).setPreferredWidth(133);
		table.getColumnModel().getColumn(3).setPreferredWidth(134);
		table.getColumnModel().getColumn(5).setPreferredWidth(45);
		table.getColumnModel().getColumn(5).setMinWidth(40);
		table.getColumnModel().getColumn(5).setMaxWidth(50);

		JLabel lblSearchResult = new JLabel("Search Result");
		lblSearchResult.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSearchResult.setBounds(433, 11, 126, 30);
		frmResultScreen.getContentPane().add(lblSearchResult);

		JLabel lblOverview = new JLabel("Overview:");
		lblOverview.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblOverview.setBounds(25, 302, 78, 21);
		frmResultScreen.getContentPane().add(lblOverview);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(135, 301, 296, 210);
		frmResultScreen.getContentPane().add(scrollPane_1);

		JTextArea textArea = new JTextArea();
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		scrollPane_1.setViewportView(textArea);
		textArea.setText(movie.getOverview());

		JLabel lblPoster = new JLabel("Poster:");
		lblPoster.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPoster.setBounds(468, 302, 67, 18);
		frmResultScreen.getContentPane().add(lblPoster);

		String path = "http://image.tmdb.org/t/p/w185/" + movie.getPosterPath();
		URL url = new URL(path);
		BufferedImage image = ImageIO.read(url);

		lblNa = new JLabel(new ImageIcon(image));
		lblNa.setBackground(SystemColor.controlHighlight);
		lblNa.setHorizontalAlignment(SwingConstants.CENTER);
		lblNa.setBounds(528, 306, 258, 271);
		frmResultScreen.getContentPane().add(lblNa);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				SearchScreen.bstScore =0;
				SearchScreen.htScore =0;
				created = false;
				SearchScreen.frmSsSearchScreen.setVisible(true);
				frmResultScreen.dispose();
			}
		});
		btnBack.setBounds(865, 554, 89, 23);
		frmResultScreen.getContentPane().add(btnBack);
		
		JLabel lblBstRating = new JLabel("BST Rating:");
		lblBstRating.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblBstRating.setBounds(25, 558, 126, 14);
		frmResultScreen.getContentPane().add(lblBstRating);
		
		JLabel lblHashtableRating = new JLabel("HashTable Rating:");
		lblHashtableRating.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblHashtableRating.setBounds(25, 583, 126, 14);
		frmResultScreen.getContentPane().add(lblHashtableRating);
		
		JLabel bstResultL = new JLabel(""+SearchScreen.bstScore+" steps");
		bstResultL.setBounds(161, 558, 78, 14);
		frmResultScreen.getContentPane().add(bstResultL);
		
		JLabel hashTableResultL = new JLabel("");
		hashTableResultL.setBounds(161, 583, 78, 14);
		frmResultScreen.getContentPane().add(hashTableResultL);
		
		JTextPane txtpnClickAMovie = new JTextPane();
		txtpnClickAMovie.setForeground(new Color(47, 79, 79));
		txtpnClickAMovie.setBackground(new Color(245, 245, 245));
		txtpnClickAMovie.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 14));
		txtpnClickAMovie.setEditable(false);
		txtpnClickAMovie.setText("Select a movie in the table above to see the additional information displayed here");
		txtpnClickAMovie.setBounds(848, 339, 135, 106);
		frmResultScreen.getContentPane().add(txtpnClickAMovie);

		DefaultTableModel model = (DefaultTableModel) table.getModel();

		// Get a set of all the entries (key - value pairs) contained in the Hashtable
		Set<String> entrySet = HT.keySet();

		// Obtain an Iterator for the entries Set
		Iterator<String> it = entrySet.iterator();

		// Iterate through Hashtable entries
		while (it.hasNext())
		{
			String key = (String) it.next();
			Movie movieOBJ = (Movie) HT.get(key);			
			model.addRow(new Object[]
			{ movieOBJ.getId(), movieOBJ.getTitle(), movieOBJ.getCharacter(), movieOBJ.getGenre(),
					movieOBJ.getReleaseDate(), movieOBJ.getRating() });
			
			//Getting a Hashatable search count for performance analysis
			if(!done)
			{
				if(movie == movieOBJ) done=true;
				else SearchScreen.htScore++;
			}
		}
		
		hashTableResultL.setText(""+SearchScreen.htScore+" steps");

		// Set the selected row to display the searched movie
		for (int i = 0; i < table.getRowCount(); i++)
		{
			if (table.getModel().getValueAt(i, 0).equals(movie.getId()))
			{	
				table.getSelectionModel().addSelectionInterval(i, i);
				table.scrollRectToVisible(table.getCellRect(i, 0, true));
			}
		}

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent event)
			{
				if (table.getSelectedRow() > -1)
				{
					Movie movieOBJ = (Movie) HT.get(table.getValueAt(table.getSelectedRow(), 1).toString());
					String path = "http://image.tmdb.org/t/p/w185/" + movieOBJ.getPosterPath();
					URL url = null;
					try
					{
						url = new URL(path);
					} catch (MalformedURLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					BufferedImage image = null;
					try
					{
						image = ImageIO.read(url);
					} catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					lblNa.setIcon(new ImageIcon(image));

					textArea.setText(movieOBJ.getOverview());
				}
			}
		});

	}

	public static ResultsScreen getInstance(Frame frame, Hashtable HT, BinaryTree Tree, Movie movie) throws IOException
	{
		if (!created)
		{
			single = new ResultsScreen(HT, Tree, movie);
			created = true;
			frame.setVisible(false);
		}
		return single;
	}
}
