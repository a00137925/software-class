import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JToggleButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.ImageIcon;
import java.awt.Window.Type;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Hashtable;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class SearchScreen
{

	private static SearchScreen single;
	private static boolean created = false;
	static JFrame frmSsSearchScreen;
	private JTextField txtSearch;
	static int bstScore=0, htScore =0;
	// Hashtable arnoldHT,
	// Hashtable stalloneHT,
	// BinaryTree arnoldTree,
	// BinaryTree stalloneTree

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args)
	// {
	// EventQueue.invokeLater(new Runnable()
	// {
	// public void run()
	// {
	// try
	// {
	// SearchScreen window = new SearchScreen();
	// window.frmSsSearchScreen.setVisible(true);
	// } catch (Exception e)
	// {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the application.
	 */
	public SearchScreen(Hashtable arnoldHT, Hashtable stalloneHT, BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		initialize(arnoldHT, stalloneHT, arnoldTree, stalloneTree);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Hashtable arnoldHT, Hashtable stalloneHT, BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		frmSsSearchScreen = new JFrame();
		frmSsSearchScreen.getContentPane().setBackground(new Color(245, 245, 245));
		frmSsSearchScreen.setResizable(false);
		frmSsSearchScreen.setType(Type.POPUP);
		frmSsSearchScreen.setTitle("S&S Search Screen");
		frmSsSearchScreen.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\schwarzenegger.png"));
		frmSsSearchScreen.setBounds(100, 100, 450, 300);
		frmSsSearchScreen.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmSsSearchScreen.getContentPane().setLayout(null);
		frmSsSearchScreen.setLocationRelativeTo(null);
		frmSsSearchScreen.setVisible(true);
		frmSsSearchScreen.addWindowListener(new java.awt.event.WindowAdapter()
		{
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent)
			{
				if (JOptionPane.showConfirmDialog(frmSsSearchScreen, "Are you sure you want to close this window?",
						"Close Window?", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
				{
					created = false;
					Dashboard.frmSsDashboard.setVisible(true);
					frmSsSearchScreen.dispose();
				}
			}
		});

		JButton btnCancel = new JButton("Back");
		btnCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				created = false;
				Dashboard.frmSsDashboard.setVisible(true);
				frmSsSearchScreen.dispose();
			}
		});
		btnCancel.setBounds(160, 128, 89, 23);
		frmSsSearchScreen.getContentPane().add(btnCancel);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Schwarzenegger");
		rdbtnNewRadioButton.setSelected(true);
		rdbtnNewRadioButton.setFont(new Font("Georgia", Font.BOLD, 11));
		rdbtnNewRadioButton.setContentAreaFilled(false);
		rdbtnNewRadioButton.setBounds(140, 72, 143, 23);
		frmSsSearchScreen.getContentPane().add(rdbtnNewRadioButton);

		JRadioButton rdbtnStallone = new JRadioButton("Stallone");
		rdbtnStallone.setFont(new Font("Georgia", Font.BOLD, 11));
		rdbtnStallone.setContentAreaFilled(false);
		rdbtnStallone.setBounds(150, 98, 109, 23);
		frmSsSearchScreen.getContentPane().add(rdbtnStallone);

		rdbtnNewRadioButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				rdbtnNewRadioButton.setSelected(true);
				rdbtnStallone.setSelected(false);
			}
		});

		rdbtnStallone.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				rdbtnStallone.setSelected(true);
				rdbtnNewRadioButton.setSelected(false);
			}
		});
		txtSearch = new JTextField();
		txtSearch.setBorder(new LineBorder(new Color(218, 165, 32), 1, true));
		txtSearch.setFont(new Font("Georgia", Font.PLAIN, 14));
		txtSearch.setBounds(120, 36, 143, 29);
		frmSsSearchScreen.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		JLabel lblKeyword = new JLabel("Search Movie Keyword");
		lblKeyword.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblKeyword.setBounds(120, 11, 147, 23);
		frmSsSearchScreen.getContentPane().add(lblKeyword);

		JButton btnGo = new JButton("GO");
		btnGo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Movie movie1 = null;
				if (txtSearch.getText().equals(""))
					JOptionPane.showMessageDialog(null, "Please Enter a Movie Title!", "Error",
							JOptionPane.ERROR_MESSAGE);
				else
				{
					if (rdbtnNewRadioButton.isSelected())
					{
						try
						{
							// Search BST
							movie1 = searchBST(txtSearch.getText().toLowerCase(), arnoldTree);
							
							try
							{
								ResultsScreen.getInstance(frmSsSearchScreen, arnoldHT, arnoldTree, movie1);
							} catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} catch (Exception er)
						{
							JOptionPane.showMessageDialog(null, "Not found. Try again."
									+ "\n-Try to use as few words as possible" + "\n-Try basic terms", "Error",
									JOptionPane.ERROR_MESSAGE);
						}

					} else
					{
						try
						{
							// Search BST
							movie1 = searchBST(txtSearch.getText().toLowerCase(), stalloneTree);
							
							try
							{
								ResultsScreen.getInstance(frmSsSearchScreen, stalloneHT, stalloneTree, movie1);
							} catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} catch (Exception er)
						{
							JOptionPane.showMessageDialog(null, "Not found. Try again."
									+ "\n-Try to use as few words as possible" + "\n-Try basic terms", "Error",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnGo.setBounds(265, 37, 54, 29);
		frmSsSearchScreen.getContentPane().add(btnGo);

		JLabel label_1 = new JLabel("");
		label_1.setIcon(
				new ImageIcon("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\schwarzenegger.png"));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		label_1.setEnabled(false);
		label_1.setBackground(new Color(169, 169, 169));
		label_1.setBounds(-132, 0, 308, 271);
		frmSsSearchScreen.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\stallone.png"));
		label_2.setEnabled(false);
		label_2.setBounds(200, 11, 308, 272);
		frmSsSearchScreen.getContentPane().add(label_2);
	}

	public static SearchScreen getInstance(Frame dashboard, Hashtable arnoldHT, Hashtable stalloneHT,
			BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		if (!created)
		{
			single = new SearchScreen(arnoldHT, stalloneHT, arnoldTree, stalloneTree);
			created = true;
			dashboard.setVisible(false);
		}
		return single;
	}

	public Movie searchBST(String key, BinaryTree tree)
	{
		Movie movie = tree.locate(key, tree.root).getMovie();
		return movie;
	}

	public Movie searchHT(String key, Hashtable ht)
	{
		return null;
	}
}
