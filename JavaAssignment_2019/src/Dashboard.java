import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;
import java.awt.Window.Type;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.Insets;
import javax.swing.border.LineBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.EtchedBorder;

public class Dashboard
{
//	There is no limit to what you can do but it must involve some type of Search/insert application using both techniques.
//
//	Can use/modify supplied Search Application Sample Code.
//
//	Functionality include, 
//	�	Insert into BSTree and Hash Table
//	�	Search using key
//	�	Expect a more complex application and better user interface
//	�	Scope to use any java feature covered on course
//	�	Counts for 30% of final mark
//	�	Ideally should not look like the application below
//	�	Should report on performance of operations (e.g. number of steps for a search)
//	�	Should use Iterator Pattern on Hashtable to allow client to iterate over collection using
//	(.iterator(), .next() & .hasNext() )


	
	public static JFrame frmSsDashboard;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Dashboard window = new Dashboard();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public Dashboard() throws Exception
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception
	{
		API api = new API();
		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		frmSsDashboard = new JFrame();
		frmSsDashboard.setType(Type.POPUP);
		frmSsDashboard.setResizable(false);
		frmSsDashboard.setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\stallone.png"));
		frmSsDashboard.setBackground(new Color(204, 153, 0));
		frmSsDashboard.setTitle("S&S Dashboard");
		frmSsDashboard.getContentPane().setForeground(new Color(204, 153, 0));
		frmSsDashboard.getContentPane().setBackground(new Color(245, 245, 245));
		frmSsDashboard.setBounds(100, 100, 450, 300);
		frmSsDashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSsDashboard.getContentPane().setLayout(null);
		frmSsDashboard.setLocationRelativeTo(null);
		frmSsDashboard.setVisible(true);

		JButton btnSearchMovies = new JButton("Search Movies");
		btnSearchMovies.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				SearchScreen.getInstance(frmSsDashboard, api.getArnoldHT(), api.getStalloneHT(), api.getArnoldTree(), api.getStalloneTree());
			}
		});
		btnSearchMovies.setFocusPainted(false);
		btnSearchMovies.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(204, 153, 0), null, null, null));
		btnSearchMovies.setMargin(new Insets(5, 14, 5, 14));
		btnSearchMovies.setForeground(new Color(47, 79, 79));
		btnSearchMovies.setBackground(new Color(211, 211, 211));
		btnSearchMovies.setFont(new Font("Georgia", Font.BOLD, 12));
		btnSearchMovies.setBounds(136, 59, 143, 32);
		frmSsDashboard.getContentPane().add(btnSearchMovies);

		JLabel lblMovieTv = new JLabel("Schwarzenegger & Stallone ");
		lblMovieTv.setForeground(new Color(0, 0, 0));
		lblMovieTv.setBackground(Color.WHITE);
		lblMovieTv.setFont(new Font("Bookman Old Style", Font.PLAIN, 17));
		lblMovieTv.setBounds(95, 11, 232, 37);
		frmSsDashboard.getContentPane().add(lblMovieTv);

		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(204, 153, 0));
		separator.setBounds(95, 71, 42, 2);
		frmSsDashboard.getContentPane().add(separator);

		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(new Color(204, 153, 0));
		separator_1.setBounds(263, 71, 58, 2);
		frmSsDashboard.getContentPane().add(separator_1);

		JLabel label = new JLabel("");
		label.setFont(new Font("Georgia", Font.PLAIN, 11));
		label.setEnabled(false);
		label.setIcon(new ImageIcon("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\stallone.png"));
		label.setBounds(190, 11, 308, 261);
		frmSsDashboard.getContentPane().add(label);

		JLabel label_1 = new JLabel("");
		label_1.setBackground(new Color(169, 169, 169));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		label_1.setEnabled(false);
		label_1.setIcon(new ImageIcon("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\schwarzenegger.png"));
		label_1.setBounds(-139, 0, 308, 272);
		frmSsDashboard.getContentPane().add(label_1);

		JButton btnAstaLaVista = new JButton("Exit");
		btnAstaLaVista.setBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(184, 134, 11), null));
		btnAstaLaVista.setBackground(new Color(192, 192, 192));
		btnAstaLaVista.setForeground(new Color(47, 79, 79));
		btnAstaLaVista.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				System.exit(0);
			}
		});
		btnAstaLaVista.setFont(new Font("Georgia", Font.BOLD, 11));
		btnAstaLaVista.setBounds(167, 151, 77, 23);
		frmSsDashboard.getContentPane().add(btnAstaLaVista);
		
		JButton btnAddMovie = new JButton("Add Movie");
		btnAddMovie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddMovieScreen.getInstance(frmSsDashboard, api.getArnoldHT(), api.getStalloneHT(), api.getArnoldTree(), api.getStalloneTree());
				
			}
		});
		btnAddMovie.setMargin(new Insets(5, 14, 5, 14));
		btnAddMovie.setForeground(new Color(47, 79, 79));
		btnAddMovie.setFont(new Font("Georgia", Font.BOLD, 12));
		btnAddMovie.setFocusPainted(false);
		btnAddMovie.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(204, 153, 0), null, null, null));
		btnAddMovie.setBackground(new Color(211, 211, 211));
		btnAddMovie.setBounds(136, 92, 143, 32);
		frmSsDashboard.getContentPane().add(btnAddMovie);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setForeground(new Color(204, 153, 0));
		separator_2.setBounds(95, 106, 42, 2);
		frmSsDashboard.getContentPane().add(separator_2);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setForeground(new Color(204, 153, 0));
		separator_3.setBounds(279, 106, 42, 2);
		frmSsDashboard.getContentPane().add(separator_3);
	}
}
