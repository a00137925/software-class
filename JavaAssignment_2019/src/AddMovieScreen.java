import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Component;
import java.awt.Toolkit;

public class AddMovieScreen
{

	private JFrame frmAddMovieScreen;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	private static AddMovieScreen single;
	private static boolean created = false;
	private JFrame frmSsSearchScreen;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args)
	// {
	// EventQueue.invokeLater(new Runnable()
	// {
	// public void run()
	// {
	// try
	// {
	// AddMovieScreen window = new AddMovieScreen();
	// window.frame.setVisible(true);
	// } catch (Exception e)
	// {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the application.
	 */
	public AddMovieScreen(Hashtable arnoldHT, Hashtable stalloneHT, BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		initialize(arnoldHT, stalloneHT, arnoldTree, stalloneTree);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Hashtable arnoldHT, Hashtable stalloneHT, BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		frmAddMovieScreen = new JFrame();
		frmAddMovieScreen.getContentPane().setBackground(new Color(245, 245, 245));
		frmAddMovieScreen.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\mrigz\\Documents\\Software\\JavaAssignment_2019\\schwarzenegger.png"));
		frmAddMovieScreen.setTitle("Add Movie Screen");
		frmAddMovieScreen.setBounds(100, 100, 542, 390);
		frmAddMovieScreen.setLocationRelativeTo(null);
		frmAddMovieScreen.setVisible(true);
		frmAddMovieScreen.getContentPane().setLayout(null);
		frmAddMovieScreen.addWindowListener(new java.awt.event.WindowAdapter()
		{
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent)
			{
				if (JOptionPane.showConfirmDialog(frmAddMovieScreen, "Are you sure you want to close this window?", "Close Window?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
				{
					created = false;
					Dashboard.frmSsDashboard.setVisible(true);
					frmAddMovieScreen.dispose();
				}
			}
		});

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(38, 68, 76, 14);
		frmAddMovieScreen.getContentPane().add(lblTitle);

		JLabel lblCharacter = new JLabel("Character:");
		lblCharacter.setBounds(38, 108, 76, 14);
		frmAddMovieScreen.getContentPane().add(lblCharacter);

		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(38, 140, 76, 20);
		frmAddMovieScreen.getContentPane().add(lblGenre);

		JLabel lblDate = new JLabel("Release Date:");
		lblDate.setBounds(38, 182, 76, 21);
		frmAddMovieScreen.getContentPane().add(lblDate);

		JLabel lblRating = new JLabel("Rating:");
		lblRating.setBounds(38, 225, 76, 22);
		frmAddMovieScreen.getContentPane().add(lblRating);

		JLabel lblNewLabel_3 = new JLabel("Overview:");
		lblNewLabel_3.setBounds(38, 258, 76, 14);
		frmAddMovieScreen.getContentPane().add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(134, 61, 167, 28);
		frmAddMovieScreen.getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(134, 100, 167, 31);
		frmAddMovieScreen.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(134, 140, 167, 28);
		frmAddMovieScreen.getContentPane().add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(134, 179, 167, 28);
		frmAddMovieScreen.getContentPane().add(textField_3);
		textField_3.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(134, 258, 167, 67);
		frmAddMovieScreen.getContentPane().add(scrollPane);

		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setBorder(new LineBorder(Color.GRAY));
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);

		JLabel lblAddYourFantasy = new JLabel("Add Your Fantasy Schwarzenegger Movie!");
		lblAddYourFantasy.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAddYourFantasy.setBounds(38, 11, 405, 31);
		frmAddMovieScreen.getContentPane().add(lblAddYourFantasy);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				created = false;
				Dashboard.frmSsDashboard.setVisible(true);
				frmAddMovieScreen.dispose();
			}
		});
		btnCancel.setBounds(323, 279, 89, 23);
		frmAddMovieScreen.getContentPane().add(btnCancel);

		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(5, null, 10, 1));
		spinner.setBounds(133, 222, 168, 25);
		frmAddMovieScreen.getContentPane().add(spinner);

		JRadioButton rdbtnSchwarzenegger = new JRadioButton("Schwarzenegger");
		rdbtnSchwarzenegger.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				lblAddYourFantasy.setText("Add Your Fantasy Schwarzenegger Movie!");
			}
		});
		rdbtnSchwarzenegger.setSelected(true);
		buttonGroup.add(rdbtnSchwarzenegger);
		rdbtnSchwarzenegger.setBounds(366, 191, 121, 23);
		frmAddMovieScreen.getContentPane().add(rdbtnSchwarzenegger);

		JRadioButton rdbtnStallone = new JRadioButton("Stallone");
		rdbtnStallone.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				lblAddYourFantasy.setText("Add Your Fantasy Stallone Movie!");
			}
		});
		buttonGroup.add(rdbtnStallone);
		rdbtnStallone.setBounds(366, 217, 121, 23);
		frmAddMovieScreen.getContentPane().add(rdbtnStallone);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (textField.equals("") || textField_1.equals("") || textField_2.equals("") || textField_3.equals("")
						|| textArea.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Please Fill All Fields!", "Error", JOptionPane.ERROR_MESSAGE);
				} else
				{
					// Movie(String title, String character, String overview, String genre, String
					// releaseDate, String posterPath, int rating)
					Movie movie1 = new Movie(arnoldTree.nodeCount(arnoldTree.getRoot())+1,textField.getText(), textField_1.getText(), textArea.getText(),
							textField_2.getText(), textField_3.getText(), "N/A", (int) spinner.getValue());
					
					Movie movie2 = new Movie(stalloneTree.nodeCount(stalloneTree.getRoot())+1,textField.getText(), textField_1.getText(), textArea.getText(),
							textField_2.getText(), textField_3.getText(), "N/A", (int) spinner.getValue());

					if (rdbtnSchwarzenegger.isSelected())
					{
						arnoldHT.put(textField.getText(), movie1);
						arnoldTree.addNode(arnoldTree.nodeCount(arnoldTree.getRoot())+1, movie1);
						JOptionPane.showMessageDialog(null, "Schwarzenegger Movie Added to Collection!", "Success!", JOptionPane.INFORMATION_MESSAGE);
						created = false;
						Dashboard.frmSsDashboard.setVisible(true);
						frmAddMovieScreen.dispose();

					} else
					{
						stalloneHT.put(textField.getText(), movie2);
						stalloneTree.addNode(stalloneTree.nodeCount(stalloneTree.getRoot())+1, movie2);
						JOptionPane.showMessageDialog(null, "Stallone Movie Added to Collection!", "Success!", JOptionPane.INFORMATION_MESSAGE);
						created = false;
						Dashboard.frmSsDashboard.setVisible(true);
						frmAddMovieScreen.dispose();
					}
					
				}
			}
		});
		btnAdd.setBounds(422, 279, 89, 23);
		frmAddMovieScreen.getContentPane().add(btnAdd);

		JLabel lblChooseActor = new JLabel("Choose Actor!");
		lblChooseActor.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblChooseActor.setBounds(366, 149, 119, 20);
		frmAddMovieScreen.getContentPane().add(lblChooseActor);

		JSeparator separator = new JSeparator();
		separator.setBounds(351, 179, 136, 2);
		frmAddMovieScreen.getContentPane().add(separator);

	}

	public static AddMovieScreen getInstance(JFrame frmSsDashboard, Hashtable arnoldHT, Hashtable stalloneHT, BinaryTree arnoldTree, BinaryTree stalloneTree)
	{
		if (!created)
		{
			single = new AddMovieScreen(arnoldHT, stalloneHT, arnoldTree, stalloneTree);
			created = true;
			frmSsDashboard.setVisible(false);
		}
		return single;

	}
}
