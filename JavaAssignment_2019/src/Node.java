class Node {

	int key;
	Movie movie;
	
	//Movie Object

	Node leftChild;
	Node rightChild;

	Node(int key, Movie movie) {

		this.key = key;
		this.movie = movie;

	}

	public String toString() {

		return movie.getTitle() + " has the key " + key;

		/*
		 * return name + " has the key " + key + "\nLeft Child: " + leftChild +
		 * "\nRight Child: " + rightChild + "\n";
		 */

	}
	
	public Movie getMovie()
	{
		return movie;
	}

}