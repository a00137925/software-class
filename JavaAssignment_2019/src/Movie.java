
public class Movie
{
	String title, character, overview, genre, releaseDate, posterPath;
	int rating, id;
	
	public Movie(int id, String title, String character, String overview, String genre, String releaseDate, String posterPath, int rating)
	{
		super();
		this.id = id;
		this.title = title;
		this.character = character;
		this.overview = overview;
		this.releaseDate = releaseDate;
		this.posterPath = posterPath;
		this.genre = genre;
		this.rating = rating;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getCharacter()
	{
		return character;
	}

	public void setCharacter(String character)
	{
		this.character = character;
	}

	public String getOverview()
	{
		return overview;
	}

	public void setOverview(String overview)
	{
		this.overview = overview;
	}

	public String getReleaseDate()
	{
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate)
	{
		this.releaseDate = releaseDate;
	}

	public String getPosterPath()
	{
		return posterPath;
	}

	public void setPosterPath(String posterPath)
	{
		this.posterPath = posterPath;
	}

	public int getRating()
	{
		return rating;
	}

	public void setRating(int rating)
	{
		this.rating = rating;
	}

	public String getGenre()
	{
		return genre;
	}

	public void setGenre(String genre)
	{
		this.genre = genre;
	}
	
	
	
//    {
//	      "character": "Douglas Quaid/Hauser",
//	      "credit_id": "52fe4283c3a36847f8024e9b",
//	      "poster_path": "/ikYpJ0AjGBNnAYFnPJDUVIOcduR.jpg",
//	      "id": 861,
//	      "video": false,
//	      "vote_count": 2732,
//	      "adult": false,
//	      "backdrop_path": "/rPqCxVXBD89jeWMgJU3MeFA6GDV.jpg",
//	      "genre_ids": [
//	        28,
//	        12,
//	        878
//	      ],
//	      "original_language": "en",
//	      "original_title": "Total Recall",
//	      "popularity": 12.704,
//	      "title": "Total Recall",
//	      "vote_average": 7.2,
//	      "overview": "Construction worker Douglas Quaid discovers a memory chip in his brain during a virtual-reality trip. He also finds that his past has been invented to conceal a plot of planetary domination. Soon, he's off to Mars to find out who he is and who planted the chip.",
//	      "release_date": "1990-06-01"
//	    },

}
