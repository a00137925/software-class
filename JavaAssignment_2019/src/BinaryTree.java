public class BinaryTree
{

	Node root;

	public void addNode(int key, Movie name)
	{

		// Create a new Node and initialize it

		Node newNode = new Node(key, name);

		// If there is no root this becomes root
		if (root == null)
		{

			root = newNode;

		} else
		{

			// Set root as the Node we will start
			// with as we traverse the tree
			Node focusNode = root;

			// Future parent for our new Node

			Node parent;

			while (true)
			{

				// root is the top parent so we start
				// there

				parent = focusNode;

				// Check if the new node should go on
				// the left side of the parent node

				if (key < focusNode.key)
				{

					// Switch focus to the left child

					focusNode = focusNode.leftChild;

					// If the left child has no children

					if (focusNode == null)
					{

						// then place the new node on the left of it

						parent.leftChild = newNode;
						return; // All Done

					}

				} else
				{ // If we get here put the node on the right

					focusNode = focusNode.rightChild;

					// If the right child has no children

					if (focusNode == null)
					{

						// then place the new node on the right of it

						parent.rightChild = newNode;
						return; // All Done

					}

				}

			}
		}

	}

	// All nodes are visited in ascending order
	// Recursion is used to go to one node and
	// then go to its child nodes and so forth

	
	public Node findNode(int key)
	{

		// Start at the top of the tree

		Node focusNode = root;

		// While we haven't found the Node
		// keep looking

		while (focusNode.key != key)
		{

			// If we should search to the left

			if (key < focusNode.key)
			{

				// Shift the focus Node to the left child

				focusNode = focusNode.leftChild;

			} else
			{

				// Shift the focus Node to the right child

				focusNode = focusNode.rightChild;

			}

			// The node wasn't found

			if (focusNode == null)
				return null;

		}

		return focusNode;

	}
	
	public Node locate(String p, Node node)
	{
		SearchScreen.bstScore++;
		Node result = null;
	    if (node == null)
	        return null;
	    if (node.getMovie().getTitle().toLowerCase().contains(p))
	        return node;
	    if (node.leftChild != null)
	        result = locate(p,node.leftChild);
	    if (result == null)
	        result = locate(p,node.rightChild);
	    return result;

	}

	// Using Recursion, count the number of Nodes in the BST
	public int nodeCount(Node root)
	{
		if (root == null)
			return 0;
		int cnt = 0;
		cnt++;
		cnt += nodeCount(root.leftChild);
		cnt += nodeCount(root.rightChild);
		return cnt;
	}
	
	public Node getRoot() {
		return root;
	}
	// public static void main(String[] args)
	// {
	//
	// // BinaryTree theTree = new BinaryTree();
	// //
	// // theTree.addNode(50, "Boss");
	// //
	// // theTree.addNode(25, "Vice President");
	// //
	// // theTree.addNode(15, "Office Manager");
	// //
	// // theTree.addNode(30, "Secretary");
	// //
	// // theTree.addNode(75, "Sales Manager");
	// //
	// // theTree.addNode(85, "Salesman 1");
	// //
	// // // Different ways to traverse binary trees
	// //
	// // // theTree.inOrderTraverseTree(theTree.root);
	// //
	// // // theTree.preorderTraverseTree(theTree.root);
	// //
	// // // theTree.postOrderTraverseTree(theTree.root);
	// //
	// // // Find the node with key 75
	// //
	// // System.out.println("\nNode with the key 75");
	// //
	// // System.out.println(theTree.findNode(75));
	//
	// }
}
