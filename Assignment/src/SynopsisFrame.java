
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*; //This is new

class SynopsisFrame extends JFrame implements ActionListener
{
	
	private JLabel l0 = new JLabel("  Show Synopsis:  ");
	private JLabel l1 = new JLabel();
	
	private JButton b1=new JButton("Return");
	
	private JPanel p1=new JPanel();
	
	private ArrayList list = new ArrayList();

	private int current;
	
//CONSTRUCTOR
	public SynopsisFrame(String s, Movies m) 
	{
		super(s);
		
		
		display(m);
		
		Container content=getContentPane();
		content.setLayout(null);
		Font f=new Font("TimesRoman", Font.BOLD,28);
		
		
		l0.setFont(f);  
		
		content.add(l0); 
		content.add(l1);
		content.add(b1);
		
		l0.setBounds(60,20,350,40);
		l1.setBounds(30,60,450,100);
		b1.setBounds(140,180,160,20);
		
		
		b1.addActionListener(this); 
		
	
		
		setSize(520,400);    setVisible(true);
	}

//CONSTRUCTOR
	public SynopsisFrame(String s, TV t) 
	{
		super(s);
		
		
		display(t);
		
		Container content=getContentPane();
		content.setLayout(null);
		Font f=new Font("TimesRoman", Font.BOLD,28);
		
		
		l0.setFont(f);  
		
		content.add(l0); 
		content.add(l1);
		content.add(b1);
		
		l0.setBounds(60,20,350,40);
		l1.setBounds(30,60,450,100);
		b1.setBounds(140,180,160,20);
		
		
		b1.addActionListener(this); 
		
	
		
		setSize(520,400);    setVisible(true);
	}
	
	public void display(Movies m)
	{
		l1.setText(m.readSyn());
	}
	
	public void display(TV t)
	{
		l1.setText(t.readSyn());
	}
	
	public void actionPerformed(ActionEvent e)
	{

	  	Object target=e.getSource();
	 	if (target==b1)
	 		{
	 		
	 			setVisible(false);
	          	         
	 		}

	    
	}

}