import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;

public class AddMovieFrame 
{

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JLabel lblSynopsis;
	
	LinkedList list;
	
	public AddMovieFrame(LinkedList list) 
	{
		
		this.list = list;

		frame = new JFrame();
		frame.setBounds(100, 100, 428, 421);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMovieTitle = new JLabel("Name");
		lblMovieTitle.setFont(new Font("Calibri", Font.ITALIC, 12));
		lblMovieTitle.setBounds(53, 34, 46, 14);
		frame.getContentPane().add(lblMovieTitle);
		
		JLabel lblNewLabel = new JLabel("Genre");
		lblNewLabel.setFont(new Font("Calibri", Font.ITALIC, 12));
		lblNewLabel.setBounds(53, 62, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Director");
		lblNewLabel_1.setFont(new Font("Calibri", Font.ITALIC, 12));
		lblNewLabel_1.setBounds(53, 87, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setFont(new Font("Calibri", Font.ITALIC, 12));
		lblYear.setBounds(231, 87, 46, 14);
		frame.getContentPane().add(lblYear);
		
		textField = new JTextField();
		textField.setBounds(95, 31, 112, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(95, 59, 112, 20);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(95, 84, 112, 20);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(263, 84, 112, 20);
		frame.getContentPane().add(textField_3);
		
		lblSynopsis = new JLabel("Synopsis");
		lblSynopsis.setFont(new Font("Calibri", Font.ITALIC, 12));
		lblSynopsis.setBounds(53, 135, 46, 14);
		frame.getContentPane().add(lblSynopsis);
		
		JButton btnAdd = new JButton("Add");
		
		btnAdd.setBounds(95, 319, 89, 23);
		frame.getContentPane().add(btnAdd);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(206, 319, 89, 23);
		frame.getContentPane().add(btnCancel);
		
		JLabel lblAddMovie = new JLabel("Add Movie");
		lblAddMovie.setBackground(Color.LIGHT_GRAY);
		lblAddMovie.setFont(new Font("Century", Font.BOLD, 24));
		lblAddMovie.setBounds(247, 24, 155, 23);
		frame.getContentPane().add(lblAddMovie);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(374, 62, -125, -5);
		frame.getContentPane().add(separator);
		
		JTextArea textArea = new JTextArea();
		textArea.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textArea.setForeground(Color.BLACK);
		textArea.setBackground(Color.WHITE);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setBounds(95, 135, 281, 77);
		
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") || textField_3.getText().equals("") || textArea.getText().equals(""))
				{
					 JOptionPane.showMessageDialog(new JFrame(), "Please fill in all fields!", "Error",
						        JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					list.insert_lastM(textField.getText(), textField_1.getText(), textField_2.getText(), Integer.parseInt(textField_3.getText()), textArea.getText());
					
					 JOptionPane.showMessageDialog(new JFrame(), "Movie Added To List!", "Complete",
						        JOptionPane.INFORMATION_MESSAGE);
					 frame.setVisible(false);
					 MainMenuFrame.mOpen = false;
				}
				
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.dispose();
			}
		});
		
		frame.getContentPane().add(textArea);
		frame.setVisible(true);
		frame.setLocation(500, 500);
	}

}
