
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*; //This is new

class TVFrame extends JFrame implements ActionListener
{
	String[] res = {"480p", "720p", "1080p"};
	/*String[] dir = {"Francis Ford Coppola", "Martin Scorsese", "Clint Eastwood", 
					"Edgar Wright", "John McTiernan", "Michel Gondry", "Ridley Scott" };
	String[] gen = {"Gangster", "Drama", "Action","Romance", "Sci-Fi", "Comedy", "Western" };*/
	
	
	
	private JLabel l0 = new JLabel("  TV Shows  ");
	private JLabel l1 = new JLabel("Name");
	private JLabel l2 = new JLabel("Genre");
	private JLabel l3 = new JLabel("Creator");
	private JLabel l4 = new JLabel("Resolution");
	private JLabel l5 = new JLabel("Total: ");
	private JLabel l6 = new JLabel("Commentary: ");
	
	private JTextField t1=new JTextField(" ",20);
	private JTextField t2=new JTextField(" ",16);
	private JTextField t3=new JTextField(" ",16);
	private JTextField t4=new JTextField("�5",16);
	
	private JCheckBox cb1=new JCheckBox();
	

	private JButton b1=new JButton("Next");
	private JButton b2=new JButton("Prev");
	private JButton b3=new JButton("First");
	private JButton b4=new JButton("Last");
	private JButton b5=new JButton("Synopsis");
	private JButton b6=new JButton("Return");
	private JButton b7=new JButton("Purchase");
	private JPanel p1=new JPanel();
	

	

	
	LinkedList list; //This is new
	
	
	static int current=0; //This is new
	
	private JComboBox c1=new JComboBox(res);
	
	MainMenuFrame mm;


//CONSTRUCTOR
	public TVFrame(String s, MainMenuFrame mm, LinkedList tvList)
	{
		super(s);
		this.mm = mm;
		this.list= tvList;
		

		t1.setEnabled(false);
		t2.setEnabled(false);
		t3.setEnabled(false);
		t4.setEnabled(false);
		
		t1.setDisabledTextColor(Color.black);
		t2.setDisabledTextColor(Color.black);
		t3.setDisabledTextColor(Color.black);
		t4.setDisabledTextColor(Color.black);
		
		display();//This is new
		
		Container content=getContentPane();
		content.setLayout(new FlowLayout());
		Font f=new Font("Georgia", Font.BOLD,20);
		Font f1=new Font("Georgia", Font.BOLD,16);
		Font f2=new Font("Georgia", Font.ITALIC,16);
		p1.setLayout(new GridLayout(12,2));
		l0.setFont(f);  l1.setFont(f2); l2.setFont(f2); l3.setFont(f2); 
		content.add(l0);
		p1.add(l1); p1.add(t1);
		p1.add(l2); p1.add(t2);
		p1.add(l3); p1.add(t3);
		
		p1.add(b1); p1.add(b2);
		p1.add(b3); p1.add(b4);
		p1.add(b5); p1.add(b6);
		p1.add(l4); p1.add(c1);
		p1.add(l6); p1.add(cb1);
		p1.add(l5); p1.add(t4);
		
		
		content.add(p1);
		content.add(b7);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		cb1.addActionListener(this);
		c1.addActionListener(this);

		   //Horiz , Vert
		setSize(500,500);    setVisible(true);}


		public void display()//This is new
		{//This is new
			TV t=list.getTV(current);
			
			//This is new
			t1.setText(t.readName());//This is new
			t2.setText(t.readGenre());//This is new
			t3.setText(t.readCreator());//This is new
			
			int a=5, b=6, c=7;
			
			if(c1.getSelectedIndex()==0)
			{
				if(cb1.isSelected())
				{
					t4.setText("�"+(a+1));
				}
				else t4.setText("�"+a);
			}
			
			if(c1.getSelectedIndex()==1)
			{
				if(cb1.isSelected())
				{
					t4.setText("�"+(b+1));
				}
				else t4.setText("�"+b);
			}
			
			if(c1.getSelectedIndex()==2)
			{
				if(cb1.isSelected())
				{
					t4.setText("�"+(c+1));
				}
				else t4.setText("�"+c);
			}
			
			
		}//This is new
		
		
		

	public void actionPerformed(ActionEvent e)
	{

	  	Object target=e.getSource();
	 	if (target==b1)
	 		{//This is new
	 			if (current < list.sizeTV()-1)//This is new
	 				current ++;//This is new

	 				display();//This is new

	          		
	         }

	     if (target==b2)
	     	{
	     		if (current > 0)
	     			current --;

	     			display();
	     	}

	    if (target==b3)
	     	{

	     			current=0;

	     			display();
	     	}

	    if (target==b4)
	     	{

	     			current=list.sizeTV()-1;

	     			display();
	     	}
	    
	    if (target==b5)
     	{

     			new SynopsisFrame("Synopsis", list.getTV(current));
     	}
	    
	    if (target==b6)
     	{
    			mm.setVisible(true);
     			this.setVisible(false);
     	}
	    
	    if (target==b7)
     	{
	    		setVisible(false);
     			new ThankYou("ThankYou", mm);
     	}
	    
	    if (target==c1)
     	{

	    	display();
     	}
	    
	    if (target==cb1)
     	{

	    	display();
     	}

	  }



 }

