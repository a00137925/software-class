import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteFrame
{

	private JFrame frame;
	
	LinkedList mList, tvList;


	public DeleteFrame(LinkedList l1, LinkedList l2)
	{
		mList = l1;
		tvList = l2;

		frame = new JFrame();
		frame.setBounds(100, 100, 399, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDeleteMovietvShow = new JLabel("Delete Movie/TV Show");
		lblDeleteMovietvShow.setFont(new Font("Georgia", Font.BOLD, 18));
		lblDeleteMovietvShow.setBounds(92, 29, 216, 21);
		frame.getContentPane().add(lblDeleteMovietvShow);
		
		JComboBox comboBox1 = new JComboBox();
		comboBox1.setBounds(42, 84, 172, 20);
		frame.getContentPane().add(comboBox1);
		
		JComboBox comboBox2 = new JComboBox();
		comboBox2.setBounds(42, 133, 172, 20);
		frame.getContentPane().add(comboBox2);
		
		JButton btnNewButton = new JButton("Delete Movie");
		
		btnNewButton.setBounds(224, 83, 101, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Delete Show");
		btnNewButton_1.setBounds(224, 132, 101, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnReturn = new JButton("Return");
		
		btnReturn.setBounds(164, 205, 89, 23);
		frame.getContentPane().add(btnReturn);
		
		for(int i=0; i<mList.sizeMovies(); i++)
		{
			comboBox1.addItem(mList.getMovie(i).readName());
		}
		for(int i=0; i<tvList.sizeTV(); i++)
		{
			comboBox2.addItem(tvList.getTV(i).readName());
		}
		
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (JOptionPane.showConfirmDialog(null, "Are you sure?", "WARNING",
				        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
				{
				    mList.deleteMovie(comboBox1.getSelectedIndex());
				    comboBox1.removeItemAt(comboBox1.getSelectedIndex());
				} 
				else {
				    // no option
				}
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (JOptionPane.showConfirmDialog(null, "Are you sure?", "WARNING",
				        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
				{
				    tvList.deleteTV(comboBox2.getSelectedIndex());
				    comboBox2.removeItemAt(comboBox2.getSelectedIndex());
				} 
				else {
				    // no option
				}
			}
		});
		
		btnReturn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				MainMenuFrame.delOpen=false;
				frame.dispose();
				
			}
		});
		
		
		frame.setVisible(true);
		frame.setLocation(500, 500);
		
	}
}
