
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*; //This is new

class MainMenuFrame extends JFrame implements ActionListener
{
	
	private JLabel l0 = new JLabel("  Mark's TV & Film Streaming Service  ");
	

	private JButton b1=new JButton("View Movies");
	private JButton b2=new JButton("View TV Shows");
	private JButton b3=new JButton("Exit");
	
	private JButton b4=new JButton("Add Movie");
	private JButton b5=new JButton("Add TV Show");
	private JButton b6=new JButton("Delete Movie/TV Show");
	
	private String password = "admin";
	JPasswordField pf = new JPasswordField();
	
	private JPanel p1=new JPanel();
	JSeparator line = new JSeparator();
	JLabel adminL = new JLabel("ADMIN OPTIONS");
	
	MovieFrame mF;
	TVFrame tF;
	
	static boolean mOpen = false;
	static boolean tvOpen = false;
	static boolean delOpen = false;
	
	LinkedList mList= new LinkedList();
	LinkedList tvList= new LinkedList();
	
	
	
//CONSTRUCTOR
	public MainMenuFrame(String s)
	{
		super(s);
		
		String godfatherSyn ="<html>The aging patriarch of an organized crime dynasty transfers "
				+ "control of his clandestine empire to his reluctant son.</html>";
		
		String convoSyn ="<html>A paranoid, secretive surveillance expert has a crisis of conscience  "
				+ "when he suspects that a couple he is spying on will be murdered.</html>";
		
		String goodfellaSyn ="<html>Henry Hill and his friends work their way up through the mob hierarchy.</html>";
		
		String bullSyn ="<html>An emotionally self-destructive boxer's journey through life, "
				+ "as the violence and temper that leads him to the top in the ring destroys his life outside it.</html>";
		
		String unforgivenSyn ="<html>Retired Old West gunslinger William Munny reluctantly takes on one  "
				+ "last job, with the help of his old partner and a young man.</html>";
		
		String joseySyn ="<html>A Missouri farmer joins a Confederate guerrilla unit and winds up  "
				+ "on the run from the Union soldiers who murdered his family.</html>";
		
		String alienSyn ="<html>After a space merchant vessel perceives an unknown transmission as distress call,  "
				+ " their landing on the source moon finds one of the crew attacked by a mysterious life-form,"
				+ "and soon realize that its life cycle has merely begun.</html>";
		
		String bladeSyn ="<html>A blade runner must pursue and try to terminate four replicants who "
				+ "stole a ship in space and have returned to Earth to find their creator. </html>";
		
		String predatorSyn ="<html>A team of commandos on a mission in a Central American jungle find  "
				+ "themselves hunted by an extraterrestrial warrior.</html>";
		
		String hardSyn ="<html>John McClane, officer of the NYPD, tries to save his wife Holly Gennaro  "
				+ "and several others that were taken hostage by German terrorist Hans Gruber"
				+ "during a Christmas party at the Nakatomi Plaza in Los Angeles. </html>";
		
		String sunshinerSyn ="<html>When their relationship turns sour, a couple undergoes a procedure   "
				+ "<br>to have each other erased from their memories. But it is only through"
				+ "<br>the process of loss that they discover what they had to begin with.</html>";
		
		String apartmentSyn ="<html>A man tries to rise in his company by letting its executives use his apartment for trysts, "
				+ "but complications and a romance of his own ensue.</html>";
		
		String brugesSyn ="<html>Guilt-stricken after a job gone wrong, hitman Ray and his partner   "
				+ "await orders from their ruthless boss in Bruges, Belgium,"
				+ "the last place in the world Ray wants to be.</html>";
		
		String shaunSyn ="<html>A man decides to turn his moribund life around by winning back his ex-girlfriend, "
				+ "reconciling his relationship with his mother, and dealing with an"
				+ "entire community that has returned from the dead to eat the living.</html>";
		
		 String wireSyn ="The Wire is a crime drama based in Baltimore, USA."
				+ "However, the show goes much deper than that.";
		
		 String sopranoSyn ="<html>The Sopranos is an epic crime drama focussing on the life of."
				+ "<br>a Jersey mob boss, Tony Soprano.</html>";
		
		 String thronesSyn ="<html>Nine noble families fight for control over the mythical lands of Westeros,"
				+ "<br>While A forgotten race returns after being dormant for thousands of years.</html>";
		
		 String arrestedSyn ="<html>Level-headed son Michael Bluth takes over family affairs after his father is imprisoned."
				+ "But the rest of his spoiled, dysfunctional family are making his job unbearable.</html>";
		
		 String badSyn ="<html>A high school chemistry teacher diagnosed with inoperable lung cancer turns to "
				+ "manufacturing and selling methamphetamine in order to secure his family's future.</html>";
		
		 String westSyn ="<html>Set at the intersection of the near future and the reimagined past, "
				+ "explore a world in which every human appetite can be indulged.</html>";
		
		 String tedSyn ="<html>Crazy sitcom about 3 priests and their housekeeper who live on Craggy Island, "
				+ "not the peaceful and quiet part of Ireland it seems!</html>";
		
		
		
		mList.insertMovie("The Godfather","Gangster", "Francis Ford Coppola",1972, godfatherSyn); 
		mList.insertMovie("The Conversation","Drama", "Francis Ford Coppola",1974, convoSyn);
		mList.insertMovie("Goodfellas","Gangster", "Martin Scorsese",1990,goodfellaSyn);
		mList.insertMovie("Raging Bull","Drama", "Martin Scorsese",1980, bullSyn);
		mList.insertMovie("Unforgiven","Western", "Clint Eastwood",1992, unforgivenSyn);
		mList.insertMovie("The Outlaw Josey Wales","Western", "Clint Eastwood",1976, joseySyn);
		mList.insertMovie("Alien","Sci-Fi", "Ridely Scott",1979, alienSyn);
		mList.insertMovie("Blade Runner","Sci-Fi", "Ridley Scott",1982, bladeSyn);
		mList.insertMovie("Predator","Action", "John McTiernan",1987, predatorSyn);
		mList.insertMovie("Die Hard","Action", "John McTiernan",1988, hardSyn);
		mList.insertMovie("Eternal Sunshine of the Spotless Mind","Romance", "Michel Gondry",2004, sunshinerSyn);
		mList.insertMovie("The Apartment","Romance", "Billy Wilder",1960, apartmentSyn);
		mList.insertMovie("In Bruges","Comedy", "Martin McDonagh",2008, brugesSyn);
		mList.insertMovie("Shaun of the Dead","Comedy", "Edgar Wright",2004, shaunSyn);
		
		tvList.insertTV("The Wire","Crime Drama", "David Simon",wireSyn);
		tvList.insertTV("The Sopranos","Crime Drama", "David Chase", sopranoSyn);
		tvList.insertTV("Game of Thrones","Fantasy", " David Benioff, D. B. Weiss", thronesSyn);
		tvList.insertTV("Arrested Development","Comedy", "Mitchell Hurwitz",arrestedSyn);
		tvList.insertTV("Breaking Bad","Crime Drama", "Vince Gilligan", badSyn);
		tvList.insertTV("Westworld","Sci-Fi", "Jonathan Nolan, Lisa Joy", westSyn);
		tvList.insertTV("Father Ted","Comedy", " Graham Linehan, Arthur Mathews", tedSyn);
		
		mF = new MovieFrame("Movies", this, mList);
		mF.setVisible(false);
		tF = new TVFrame("TV", this, tvList);
		tF.setVisible(false);
		
		Container content=getContentPane();
		content.setLayout(null);
		Font f=new Font("TimesRoman", Font.BOLD,20);
		
		
		l0.setFont(f);  
		content.add(l0); 
		content.add(b1);
		content.add(b2);
		content.add(b3);
		content.add(line);
		content.add(adminL);
		content.add(b4);
		content.add(b5);
		content.add(b6);
		l0.setBounds(30,20,350,40);
		b1.setBounds(110,80,160,20);
		b2.setBounds(110,110,160,20);
		b3.setBounds(110,140,160,20);
		line.setBounds(100, 170, 180, 10);
		adminL.setBounds(150, 172, 160, 20);
		b4.setBounds(110,195,160,20);
		b5.setBounds(110,220,160,20);
		b6.setBounds(110,250,160,20);
		
		
		b1.addActionListener(this); 
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
	
		
		setSize(440,500);    setVisible(true);
	}

		
	
	public void actionPerformed(ActionEvent e)
	{

	  	Object target=e.getSource();
	 	if (target==b1)
	 		{
	 			setVisible(false);
	 			mF.repaint();
	 			mF.validate();
	 			mF.setVisible(true);
	          	         
	 		}

	     if (target==b2)
	     	{
		    	 setVisible(false);
		    	 tF.repaint();
		    	 tF.validate();
		    	 tF.setVisible(true);
	     	}

	    if (target==b3)
	     	{

	    		System.exit(0);
	     	}
	    
	    if (target==b4)
     	{
	    	int x = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

	    	if (x == JOptionPane.OK_OPTION) 
	    	{
	    	  if(password.equals(new String(pf.getPassword())))
	    	  {
	    		  System.out.println("You entered: " + password);
	    	
	    		  if(mOpen) return;
	    		  else 
	    		  {
	    			  new AddMovieFrame(mList);
	    			  mOpen=true;
	    		  }
	    	  }
	    	  else JOptionPane.showMessageDialog(new JFrame(), "Wrong Password!", "Error",
				        JOptionPane.ERROR_MESSAGE);
	    	}
     	}
	    
	    if (target==b5)
     	{
	    	int x = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

	    	if (x == JOptionPane.OK_OPTION) 
	    	{
	    	  if(password.equals(new String(pf.getPassword())))
	    	  {
	    		  System.out.println("You entered: " + password);
	    	
	    		  if(tvOpen) return;
	    		  else 
	    		  {
	    			  new AddTVFrame(tvList);
	    			  tvOpen=true;
	    		  }
	    	  }
	    	  else JOptionPane.showMessageDialog(new JFrame(), "Wrong Password!", "Error",
				        JOptionPane.ERROR_MESSAGE);
	    	}
     	}
	    
	    if (target==b6)
     	{
	    	int x = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

	    	if (x == JOptionPane.OK_OPTION) 
	    	{
	    	  if(password.equals(new String(pf.getPassword())))
	    	  {
	    		  System.out.println("You entered: " + password);
	    	
	    		  if(delOpen) return;
	    		  else 
	    		  {
	    		  	new DeleteFrame(mList, tvList);
	    		  	delOpen=true;
	    		  }
	    	  }
	    	  else JOptionPane.showMessageDialog(new JFrame(), "Wrong Password!", "Error",
				        JOptionPane.ERROR_MESSAGE);
	    	}
     	}

	}

}

