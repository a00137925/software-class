
public class TV 
{
	private String name, genre, creator;
	public TV next;
	public Synopsis synopsis;
	
	public TV(String nm, String gen, String cre, String syn)
	{
		name=nm;
		genre=gen;
		creator=cre;
		synopsis = new Synopsis(syn);
	}
	
	public String readName()
	{
		return name;
	}
	
	public String readGenre()
	{
		return genre;
	}
	
	public String readCreator()
	{
		return creator;
	}
	
	public String readSyn()
	{
		return synopsis.readName();
	}

}
