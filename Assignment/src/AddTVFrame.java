import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddTVFrame
{

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	LinkedList list;

	public AddTVFrame(LinkedList list)
	{
		
		this.list = list;
	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAddTvShow = new JLabel("Add TV Show");
		lblAddTvShow.setFont(new Font("Georgia", Font.BOLD, 18));
		lblAddTvShow.setBounds(237, 37, 168, 30);
		frame.getContentPane().add(lblAddTvShow);
		
		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(32, 48, 46, 14);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblNewLabel = new JLabel("Genre:");
		lblNewLabel.setBounds(32, 73, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblWriter = new JLabel("Writer:");
		lblWriter.setBounds(32, 98, 46, 14);
		frame.getContentPane().add(lblWriter);
		
		JLabel lblSynopsis = new JLabel("Synopsis:");
		lblSynopsis.setBounds(32, 123, 46, 14);
		frame.getContentPane().add(lblSynopsis);
		
		textField = new JTextField();
		textField.setBounds(85, 48, 108, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(85, 73, 108, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(85, 98, 108, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JTextArea textArea = new JTextArea();
		textArea.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textArea.setBounds(88, 123, 272, 73);
		frame.getContentPane().add(textArea);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") ||  textArea.getText().equals(""))
				{
					 JOptionPane.showMessageDialog(new JFrame(), "Please fill in all fields!", "Error",
						        JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					list.insert_lastTV(textField.getText(), textField_1.getText(), textField_2.getText(), textArea.getText());
					
					 JOptionPane.showMessageDialog(new JFrame(), "Show Added To List!", "Complete",
						        JOptionPane.INFORMATION_MESSAGE);
					 frame.setVisible(false);
					 MainMenuFrame.mOpen = false;
				}
			}
		});
		
		
		btnAdd.setBounds(85, 227, 89, 23);
		frame.getContentPane().add(btnAdd);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(271, 227, 89, 23);
		frame.getContentPane().add(btnCancel);
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frame.dispose();
			}
		});
		
		frame.setVisible(true);
		frame.setLocation(500, 500);
	}
}
