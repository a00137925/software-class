
public class Movies 
{
	private String name, genre, director;
	private int year;
	public Movies next;
	public Synopsis synopsis;
	
	public Movies(String nm, String gen, String dir, int yr, String syn)
	{
		name=nm;
		genre=gen;
		director=dir;
		year=yr;
		synopsis = new Synopsis(syn);
	}
	
	public String readName()
	{
		return name;
	}
	
	public String readGenre()
	{
		return genre;
	}
	
	public String readDirector()
	{
		return director;
	}
	
	public int readYear()
	{
		return year;
	}
	
	public String readSyn()
	{
		return synopsis.readName();
	}
	
	public void print()
	{
		System.out.println("Name: "+name);
		System.out.println("Genre: "+genre);
		System.out.println("Director: "+director);
		System.out.println("Year: "+year);
	}

}
