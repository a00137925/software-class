
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.*; //This is new

class MovieFrame extends JFrame implements ActionListener
{
	String[] res = {"480p", "720p", "1080p"};
	private int movIndex;	
	
	private JLabel l0 = new JLabel("  Movie List  ");
	private JLabel l1 = new JLabel("Name");
	private JLabel l2 = new JLabel("Genre");
	private JLabel l3 = new JLabel("Director");
	private JLabel l4 = new JLabel("Year");
	private JLabel l5 = new JLabel("Resolution: ");
	private JLabel l6 = new JLabel("Total: ");
	private JLabel l7 = new JLabel("Commentary: ");
	
	private JTextField t1=new JTextField(" ",20);
	private JTextField t2=new JTextField(" ",16);
	private JTextField t3=new JTextField(" ",16);
	private JTextField t4=new JTextField(" ",16);
	private JTextField t5=new JTextField("�2",16);
	
	private JCheckBox cb1=new JCheckBox();
	

	private JButton b1=new JButton("Next");
	private JButton b2=new JButton("Prev");
	private JButton b3=new JButton("First");
	private JButton b4=new JButton("Last");
	private JButton b5=new JButton("Synopsis");
	private JButton b6=new JButton("Return");
	private JButton b7=new JButton("Purchase");
	private JPanel p1=new JPanel();
	

	
	
	
	public  LinkedList list;
	
	
	static int current=0; 
	
	private JComboBox c1=new JComboBox(res);
	
	MainMenuFrame mm;

	

	//CONSTRUCTOR
	public MovieFrame(String s, MainMenuFrame mm, LinkedList mList)
	{
		super(s);
		this.mm = mm;
		this.list = mList;

		t1.setEnabled(false);
		t2.setEnabled(false);
		t3.setEnabled(false);
		t4.setEnabled(false);
		t5.setEnabled(false);
		
		t1.setDisabledTextColor(Color.black);
		t2.setDisabledTextColor(Color.black);
		t3.setDisabledTextColor(Color.black);
		t4.setDisabledTextColor(Color.black);
		t5.setDisabledTextColor(Color.black);
	
		
		display();//This is new
		
		Container content=getContentPane();
		content.setLayout(new FlowLayout());
		Font f=new Font("Georgia", Font.BOLD,20);
		Font f1=new Font("Georgia", Font.BOLD,16);
		Font f2=new Font("Georgia", Font.ITALIC,16);
		p1.setLayout(new GridLayout(12,2));
		l0.setFont(f);  l1.setFont(f2); l2.setFont(f2); l3.setFont(f2); l4.setFont(f2);
		content.add(l0);
		p1.add(l1); p1.add(t1);
		p1.add(l2); p1.add(t2);
		p1.add(l3); p1.add(t3);
		p1.add(l4); p1.add(t4);
		p1.add(b1); p1.add(b2);
		p1.add(b3); p1.add(b4);
		p1.add(b5); p1.add(b6);
		p1.add(l5); p1.add(c1);
		p1.add(l7); p1.add(cb1);
		p1.add(l6); p1.add(t5);
		//p1.add(b7);
		
		content.add(p1);
		content.add(b7);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		c1.addActionListener(this);
		cb1.addActionListener(this);

		   //Horiz , Vert
		setSize(500,600);    setVisible(true);
		
	}


		public MovieFrame()
	{
		// TODO Auto-generated constructor stub
	}


		public void display()
		{
			Movies m=list.getMovie(current);
			
			
			t1.setText(m.readName());
			t2.setText(m.readGenre());
			t3.setText(m.readDirector());
			t4.setText(""+m.readYear());
			
			int a=2, b=3, c=4;
			
			if(c1.getSelectedIndex()==0)
			{
				if(cb1.isSelected())
				{
					t5.setText("�"+(a+1));
				}
				else t5.setText("�"+a);
			}
			
			if(c1.getSelectedIndex()==1)
			{
				if(cb1.isSelected())
				{
					t5.setText("�"+(b+1));
				}
				else t5.setText("�"+b);
			}
			
			if(c1.getSelectedIndex()==2)
			{
				if(cb1.isSelected())
				{
					t5.setText("�"+(c+1));
				}
				else t5.setText("�"+c);
			}
		}
		
		
	public void actionPerformed(ActionEvent e){

	  	Object target=e.getSource();
	 	if (target==b1)
	 		{
	 			if (current < list.sizeMovies()-1)
	 				current ++;

	 				display();

	         }
	 	
	 	if (target==c1)
     	{
     		display();
     	}

	 	if (target==cb1)
     	{
     		display();
     	}
	 	
	     if (target==b2)
	     	{
	     		if (current > 0)
	     			current --;

	     			display();
	     	}

	    if (target==b3)
	     	{

	     			current=0;

	     			display();
	     	}

	    if (target==b4)
	     	{

	     			current=list.sizeMovies()-1;

	     			display();
	     	}
	    
	    if (target==b5)
     	{

     			new SynopsisFrame("Show Synopsis", list.getMovie(current));
     	}
	    
	    if (target==b6)
     	{
	    		mm.setVisible(true);
     			this.setVisible(false);
     	}
	    
	    if (target==b7)
     	{
	    		setVisible(false);
     			new ThankYou("ThankYou", mm);
     	}

	}
	 

}

