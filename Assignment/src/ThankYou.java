
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;



class ThankYou extends JFrame implements ActionListener
{
	
	private JLabel l0 = new JLabel("  Thank You!  ");
	

	
	private JButton b1=new JButton("Exit");
	
	MainMenuFrame mm;
	
	
	
//CONSTRUCTOR
	public ThankYou(String s, MainMenuFrame mm)
	{
		super(s);
		this.mm = mm;
		Container content=getContentPane();
		content.setLayout(null);
		Font f=new Font("TimesRoman", Font.BOLD,26);
		
		
		l0.setFont(f);  
		content.add(l0); 
		content.add(b1);
		
		l0.setBounds(85,20,350,40);
		b1.setBounds(90,80,160,20);
		
		
		
		b1.addActionListener(this); 
	
	
		
		setSize(350,200);    setVisible(true);
	}

		
	
	public void actionPerformed(ActionEvent e)
	{

	  	Object target=e.getSource();
	 	if (target==b1)
	 		{
	 		
	 			setVisible(false);
	 			mm.setVisible(true);;
	          	         
	 		}

	   

	}

}