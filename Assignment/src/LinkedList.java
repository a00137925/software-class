

class LinkedList
{
    private Movies firstM, nextM;
    private TV firstTV, nextTV;
    
    public  LinkedList()
    {
    	firstM=null;
    	firstTV=null;
    }

	public void insertMovie(String nm, String gen, String dir, int yr, String syn)
    {      
		Movies temp =new Movies(nm, gen, dir, yr, syn);
		temp.next=firstM;
		firstM=temp;
	}
	
	public  void insertTV(String nm, String gen, String writer, String syn)
    {      
		TV temp =new TV(nm, gen, writer, syn);
		temp.next=firstTV;
		firstTV=temp;
	}


	public  void insert_lastM(String nm, String gen, String dir, int yr, String syn)
    {      
		Movies new1 =new Movies(nm, gen, dir, yr, syn);
		Movies temp = firstM;
		
		while(temp.next!=null)
		{			
			temp = temp.next; 
		}
		temp.next = new1;

		
    }
	
	public  void insert_lastTV(String nm, String gen, String writer, String syn)
    {      
		TV new1 =new TV(nm, gen, writer, syn);
		TV temp = firstTV;
		
		while(temp.next!=null)
		{			
			temp = temp.next; 
		}
		temp.next = new1;

		
    }
	

	public  boolean searchMovies(String s1)
    {     
		Movies temp=firstM;
		
		while(temp!=null) 
		{
           if(s1.equals(temp.readName())) return true;
           temp=temp.next;
        }
		return false; 
	}
	
    /* Given a reference (pointer to pointer) to the head of a list 
    and a position, deletes the node at the given position */
	 void deleteMovie(int position) 
	 { 
	     // If linked list is empty 
	     if (firstM == null) 
	         return; 
	
	     // Store head node 
	     Movies temp = firstM; 
	
	     // If head needs to be removed 
	     if (position == 0) 
	     { 
	    	 firstM = temp.next;   // Change head 
	         return; 
	     } 
	
	     // Find previous node of the node to be deleted 
	     for (int i=0; temp!=null && i<position-1; i++) 
	         temp = temp.next; 
	
	     // If position is more than number of ndoes 
	     if (temp == null || temp.next == null) 
	         return; 
	
	     // Movies temp->next is the node to be deleted 
	     // Store pointer to the next of node to be deleted 
	     Movies next = temp.next.next; 
	
	     temp.next = next;  // Unlink the deleted node from list 
	 }
	 
	 
    /* Given a reference (pointer to pointer) to the head of a list 
    and a position, deletes the node at the given position */
	 void deleteTV(int position) 
	 { 
	     // If linked list is empty 
	     if (firstTV == null) 
	         return; 
	
	     // Store head node 
	     TV temp = firstTV; 
	
	     // If head needs to be removed 
	     if (position == 0) 
	     { 
	    	 firstTV = temp.next;   // Change head 
	         return; 
	     } 
	
	     // Find previous node of the node to be deleted 
	     for (int i=0; temp!=null && i<position-1; i++) 
	         temp = temp.next; 
	
	     // If position is more than number of ndoes 
	     if (temp == null || temp.next == null) 
	         return; 
	
	     // Movies temp->next is the node to be deleted 
	     // Store pointer to the next of node to be deleted 
	     TV next = temp.next.next; 
	
	     temp.next = next;  // Unlink the deleted node from list 
	 }



	public  int countMovies(String s1)
    {    
		int count=0;
		Movies temp=firstM;
		
		while(temp!=null) 
		{
           if(s1.equals(temp.readName())) count++;
           temp=temp.next;
        }
           
	
		return count; 
	}


     public void printMovieList()
     {
       Movies temp=firstM;
       System.out.println("\nList:");
       System.out.print("firstM->");
       
       while(temp!=null) 
       {
           temp.print();
           temp=temp.next;
       }
           
        System.out.print("NULL");   
           
     }

     public Movies getMovie(int index) 
     { 
         Movies current = firstM; 
         int count = 0; /* index of Node we are 
                           currently looking at */
         while (current != null) 
         { 
             if (count == index) 
                 return current; 
             count++; 
             current = current.next; 
         } 
   
         /* if we get to this line, the caller was asking 
         for a non-existent element so we assert fail */
         assert(false); 
         return null; 
     }
     
     public TV getTV(int index) 
     { 
         TV current = firstTV; 
         int count = 0; /* index of Node we are 
                           currently looking at */
         while (current != null) 
         { 
             if (count == index) 
                 return current; 
             count++; 
             current = current.next; 
         } 
   
         /* if we get to this line, the caller was asking 
         for a non-existent element so we assert fail */
         assert(false); 
         return null; 
     }

	public int sizeMovies()
	{
		int size=0;
		Movies temp=firstM;
       
        while(temp!=null) 
        {
           temp=temp.next;
           size++;
        }
        return size;
	} 
	
	public int sizeTV()
	{
		int size=0;
		TV temp=firstTV;
       
        while(temp!=null) 
        {
           temp=temp.next;
           size++;
        }
        return size;
	}
}


   