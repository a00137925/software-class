package examTypeQ2;

public class PairDice
{
	Die die1;
	Die die2;
	
	public PairDice(int x, int y)
	{
		die1 = new Die(x);
		die2 = new Die(y);
	}
	
	void rollDice()
	{
		die1.roll();
		die2.roll();
	}
	
	int sumDiceValue()
	{
		return die1.getValue()+die2.getValue();
	}
	
	boolean valuesEqual()
	{
		if(die1.getValue()==die2.getValue()) return true;
		else return false;
	}
	
	int readValue1()
	{
		return die1.getValue();
	}
	
	int readValue2()
	{
		return die2.getValue();
	}

}
