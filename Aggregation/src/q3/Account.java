package q3;

public class Account
{
	String     name; 
	int   number;
	int   transactions;
	Money   balance;
	
	public Account(String name, int num)
	{
		this.name = name;
		number = num;
		
		transactions =0;
		balance = new Money(0,0);
	}
	
	String read_bal()
	{
		return balance.read_value();
	}
	
	String read_name()
	{
		return name;
	}
	
	int    read_transactions()
	{
		return transactions;
	}
	
	void   deposit(int e, int c) 
	{
		transactions++;
		balance.add(e, c);
	}
	
	boolean withdraw(int e, int c)
	{
		transactions++;
		return balance.sub(e, c);
		
	}



}
