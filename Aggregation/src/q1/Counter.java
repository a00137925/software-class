package q1;

class Counter
{
	
	private int value=0;
   
	public Counter()
    {
    	value=0;
    }
    
    public Counter(int v)
    {
    	value=v;
    }
	
    public void increment()
	{
		this.value++;
	}
    
    public void win()
   	{
   		this.value+=3;
   	}
    
    public void draw()
   	{
   		this.value++;
   	}
    
    public void loss()
   	{
   		this.value+=0;
   	}
	
	public void decrement()
	{
		this.value--; 
		
	}
	
	public int read_value()
	{
		return this.value;
	}
}


