package q1;

public class Team 
{
	Counter games_played;
	Counter points;
	String team_name;
	
	public Team(String n, int gp, int pt)
	{
		team_name = n;
		games_played = new Counter(gp);
		points = new Counter(pt);
		
	}
	
	void win()
	{
		games_played.increment();
		points.win();
	}
	
	void draw()
	{
		games_played.increment();
		points.draw();
	}
	
	void loss()
	{
		games_played.increment();
	}
	
	void print_details()
	{
		System.out.println("Team: "+team_name+"\nGames Played: "+games_played.read_value()+"\nPoints: "+points.read_value());
	}
	
	String readName()
	{
		return team_name;
	}
	
	int readPlayed()
	{
		return games_played.read_value();
	}
	
	int readPoints() 
	{
		return points.read_value();
		
	}
	
	

}
