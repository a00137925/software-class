package examTypeQ1;

public class Die 
{
	int value;
	
	public Die(int v)
	{
		value = v;
	}
	
	void roll()
	{
		value = 1 + ((int)(Math.random()*1000)/6);
	}
	
	int getValue()
	{
		return value;
	}

}
