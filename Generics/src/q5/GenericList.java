package q5;

class GenericList<T extends Number & Comparable>
{
	private T [] myList;
	private int elements;
	private int max;

	public GenericList(int max)
	{
		this.max=max;
		elements=0;
	    myList=(T[])new Number[max];
	}

	 public boolean add(T el) 
	 { 
       if (elements==max)     return  false;
       else
        { 
    	   myList[elements]=el;
                elements++;
                return true; 
        }
      }

	 public T readElement(int i)
	 {
		    if(elements==0) return null;
		    T res = myList[elements-i]; 	
		 	return res;
	 }
	 public T removeLast()
	 {
		 if(elements>0)
		 {
			T res = myList[elements-1]; 	
		 	elements--;
		 	return res;
		 }
		 else return null;
		 
	 }

	 public void printElements()
	 {
		 System.out.print("[");
		 if(elements>0) System.out.print(myList[0]);
		 
		 for(int i =1; i<elements; i++)
		 {
			 
			 System.out.print(","+myList[i]); 
		 }
		 
		 System.out.print("]");
		 

	 }
	 
}
