package q5;
import java.util.Scanner;

import q3.Console;




public class TestGenericListQ 
{
	public static void main(String [] args)
	{
		Scanner sc = new Scanner(System.in);
	GenericList<Integer> gl=new GenericList<Integer>(12);

	Integer value;

	int choice=0;
	while (choice!=5){
			System.out.println();
    	System.out.println("Menu:");
		System.out.println("1: Insert at Last Position");
		System.out.println("2: Read an Element");
		System.out.println("3: Print_list");
        System.out.println("4: Remove last Element");
		System.out.println("5: Exit \n");
		
		System.out.print("Enter Choice: ");
		choice =sc.nextInt();
		switch(choice){
			    case 1:
			         value=Console.readInt("Enter a value:");
			         boolean res1=gl.add(value);
			         if (res1==false) {System.out.println("List is Full");}
				       break;
			    case 2:
					System.out.print("Enter an Index:: ");
					int index =sc.nextInt();
			         value=gl.readElement(index);
			         if (value==null){System.out.println("Error Invalid Index");}
			         else            { System.out.println("Value ["+ index+"] ="+value);}
				       break;

			    case 3:   gl.printElements();
				        break;

			    case 4:
			         value = gl.removeLast();
			         if (value==null){System.out.println("Error list empty");}
			         else            { System.out.println("Value removed ="+value);}
				       break;
		}
	}

}}
