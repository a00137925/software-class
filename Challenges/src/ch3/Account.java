package ch3;

public class Account 
{
	private String name;
	private int bal;
	
	public Account(String name, int bal)
	{
		this.name = name;
		this.bal = bal;
	}
	
	void deposit(int v)
	{
		this.bal += v;
	}
	
	boolean withdraw(int v)
	{
		if(v <= bal)
		{
			this.bal -= v;
			return true;
		}
		else return false;
	}
	
	int readBalance()
	{
		return bal;
	}

}
