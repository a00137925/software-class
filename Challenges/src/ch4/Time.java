package ch4;

public class Time 
{
	private int hr, min, sec;
	
	
	public Time(int h, int m, int s)
	{
		hr=h;
		min=m;
		sec=s;
	}
	
	void incr_sec() 
	{
		if(sec<59) sec++;
		else
		{
			incr_min();
			sec=0;
		}
	}
	
	void incr_min()
	{
		if(min<59) min++;
		else
		{
			incr_hr();
			min=0;
		}
	}
	
	void incr_hr()
	{
		if(hr<24) hr++;
		else
		{
			hr=0;
		}
	}
	
	int total_in_sec()
	{
		return (hr*3600)+(min*60)+sec;
	}
	
	void print()
	{
		System.out.println("Time = "+hr+":"+min+":"+sec);
	}
	
	

}
