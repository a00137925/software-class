package ch1;

import java.io.Serializable;

public class Counter implements Serializable
{
	private int value;
    
	public Counter()
	{
		value=0;
	}
	
	public Counter(int v)
	{
		value=v;
	}
	
	public void increment()
	{
		this.value++;
	}
	
	public void decrement()
	{
		this.value--;
	}
	
	public int read_value()
	{
		return this.value;
	}
	
	public void reset()
	{
		this.value = 0;
	}
}
