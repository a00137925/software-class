package ch1;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

//package ch1;



public class TestCounter {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		Counter c1 = new Counter();
		Counter c2 = new Counter(2);
		
		try
		{
			ObjectInputStream in=new ObjectInputStream(
			                                        new FileInputStream("test.dat"));
			c2=(Counter)in.readObject();
			in.close();
		}		
		catch(Exception e){}

		
		

		int choice = 1;
		while(choice!=5)
			{
			System.out.println();
			System.out.println("1.Increment");
			System.out.println("2.Decrement");
			System.out.println("3.Read Value");
			System.out.println("4.Reset Value");
			System.out.println("5.Exit");

			System.out.print("Please enter a value-->");
			choice =sc.nextInt();
		
			if(choice==1)
			{
				c2.increment();
			}
			else if(choice==2)
			{
				c2.decrement();

			}
			else if(choice==3)
			{
				int res=c2.read_value();
				System.out.println("Value = "+res);
			}
			
			else if(choice==4)
			{
				c2.reset();
				int res=c2.read_value();
				System.out.println("Value = "+res);
			}

		}  // end while
		
		try
		{
			ObjectOutputStream out=new ObjectOutputStream(
			                                        new FileOutputStream("test.dat"));
			out.writeObject(c2);
			out.close();
		}		
		catch(Exception e){}

	}

}
