package q1;

import q2.Console;

public class Count
{
	public static int countNum(int num)
	{
		
		if(num!=0)
    	{   
    		int last = num%10;
    		int rest = num/10;
    		return 1+countNum(rest);
    	}
    	else return 0;
    	
	}

	public static void main(String[] args)
	{
		int number=Console.readInt("Enter Value:");
	     System.out.println("Res= "+ countNum(number));

	}

}
