package q2;
public class AddEvenTest 
{

    public static int addEven(int value)
    {
    	if(value>10)
    	{   
    		if((value%10)%2==0)	
    		return (value%10)+addEven(value/10);
    	
    		else return addEven(value/10);
    	}
    	else if(value%2==0)	return value;
    	else return 0;
    	
    }
    
    public static void main(String[] args) 
    {
	  int number=Console.readInt("Enter Value:");
      System.out.println("Res= "+ addEven(number));
       
    }
}

