
public class MoreFunctions implements Visitor
{
	private Counter cc; 		
	
	public void visit(Visitable c)
	{
		cc=(Counter)c;
	}

	
	public void increment()
	{
		cc.setValue(cc.readValue()+1);
	}


}
