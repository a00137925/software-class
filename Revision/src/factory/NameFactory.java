package factory;

class NameFactory {
	// returns an instance of LastFirst or FirstFirst
	// depending on whether a comma is found
	public Namer getNamer(String entry) {
		if(entry.contains(",")) {
			int i = entry.indexOf(","); // comma determines orde
			if (i > 0)
				return new LastFirst(entry); // return one class
			else
				return new FirstFirst(entry); // or the other
		}
		else return new TitledName(entry);
	}
}
