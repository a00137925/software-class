package factory;

abstract class Namer {
	protected String last; // store last name here
	protected String first; // store first name here

	public String getFirst() {
		return first; // return first name
	}

	public String getLast() {
		return last; // return last name
	}

	abstract String readFullName();
}

// ---------------------------------------------

class FirstFirst extends Namer { // split first last
	public FirstFirst(String s) {
		int i = s.indexOf(" "); // find sep space
		if (i > 0) {
			// left is first name
			first = s.substring(0, i).trim();
			// right is last name
			last = s.substring(i + 1).trim();
		} else {
			first = ""; // put all in last name
			last = s; // if no space
		}
	}

	public String readFullName() {
		return first + " " + last;
	}
}

// --------------------------------------------------

class LastFirst extends Namer { // split last, first
	public LastFirst(String s) {
		int i = s.indexOf(","); // find comma
		if (i > 0) {
			// left is last name
			last = s.substring(0, i).trim();
			// right is first name
			first = s.substring(i + 1).trim();
		} else {
			last = s; // put all in last name
			first = ""; // if no comma
		}
	}

	public String readFullName() {
		return first + " " + last;
	}
}

class TitledName extends Namer {

		public TitledName(String s) {
			String[] list = s.split(" ");
			first = list[1].trim();
			int i = s.lastIndexOf(" ");
			last = s.substring(i, s.length()).trim();
					

		}

		public String readFullName() {
			return first + " " + last;
		}
	}

