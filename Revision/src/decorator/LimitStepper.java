package decorator;

public class LimitStepper extends Decorator{
	
	public LimitStepper(Stepper step) {
		super(step);

	}

	public void stepUp(int amt) {
		if(stepper.readValue() + amt >= 20) System.out.println("Goes Beyond Limit!!");
		else stepper.stepUp(amt);
		
	}

	public void stepDown(int amt) {
		if(stepper.readValue() - amt <= 0) System.out.println("Goes Below Limit!!");
		else stepper.stepDown(amt);

	}

	public int readValue() {
		return stepper.readValue();

	}
}


