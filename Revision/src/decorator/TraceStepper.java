package decorator;

public class TraceStepper extends Decorator {

	public TraceStepper(Stepper step) {
		super(step);

	}

	public void stepUp(int amt) {
		stepper.stepUp(amt);
		System.out.println("Stepped UP");
	}

	public void stepDown(int amt) {
		stepper.stepDown(amt);
		System.out.println("Stepped DOWN");
	}

	public int readValue() {
		return stepper.readValue();

	}
}
