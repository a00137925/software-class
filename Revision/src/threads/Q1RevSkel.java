package threads;

class Number implements Runnable{
	public int value = 0;

	public Number(int nm) {
		value = nm;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Number" + ": " + value);
			value++;
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}
		
	}
}

class Letter implements Runnable{
	public char value = 'A';

	public Letter(char nm) {
		value = nm;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Letter" + ": " + value);
			value++;
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}
		
	}
}

public class Q1RevSkel {
	public static void main(String[] args) {
		Number d1 = new Number(1);
		Letter d2 = new Letter('A');
		Thread t1 = new Thread(d1);
		Thread t2 = new Thread(d2);
		t1.start();
		t2.start();
	}
}
