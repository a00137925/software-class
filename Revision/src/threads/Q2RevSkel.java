package threads;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;



class Letter2  extends Thread implements ActionListener{
	private int counter;
	private boolean suspendFlag=false;
	private JFrame f1=new JFrame();
	private  JLabel l=new JLabel();
	private JButton suspendBtn = new JButton("Suspend");
	private JButton resumeBtn = new JButton("Resume");
	private Font fnt=new Font("TimesNewRoman",Font.BOLD,16);
	Container content;
	Letter2(){
		counter=65;
		content=f1.getContentPane();
		l.setFont(fnt);
		l.setText("Counter:  "+0);
       	             f1.setLayout(new FlowLayout());
                          f1.setSize(180,180);
                          content.add(l);
                          content.add(suspendBtn);	
                          content.add(resumeBtn);	
                          resumeBtn.addActionListener(this);
                          suspendBtn.addActionListener(this);
                          f1.setVisible(true); }


	@Override
	public void run() {
		while(counter <1000)
        {          counter++;
                   l.setFont(fnt);
                   l.setText("Letter:  "+(char)counter+"   ");
                   try{
                	   Thread.sleep(1000);
                	   synchronized(this) {
                		   while(suspendFlag) wait();
                		   }
                	   }
                   catch(Exception e){}}
		
	}
	
	public void mySuspend() {
		suspendFlag=true;
	}
	
	public synchronized void myResume() {
		suspendFlag = false;
		notify();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		Object target=e.getSource();
		if(target==suspendBtn) this.mySuspend();
		if(target==resumeBtn) this.myResume();
		
	} }   
       
public class Q2RevSkel{
public static void main(String[] args)
{
                   Letter2 c=new Letter2();
                   c.start();
                   }
}

