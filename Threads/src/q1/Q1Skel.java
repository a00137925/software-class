package q1;

class Display extends Thread   //Extends Thread
{
	public String name;
	
	public Display(String nm){
		        name=nm;
	}     
      public void run()			//Change to "run"
      {
		for(int i=0; i<4;i++)
		{ 
			System.out.println(name+": "+i);
			   try{ Thread.sleep(1000);}
		         catch(Exception e){}
		       }
      }
}  


public class Q1Skel
{
	public static void main(String[] args)
    {
            Display d1=new Display("First");
            Display d2=new Display("Second");
            d1.start();			//Call the 'start' function which automatically begins the run thread
            d2.start();			//Call the 'start' function which automatically begins the run thread
      }
}
