package q1;

class Display2 implements Runnable     //This is used because of Java's limited use of inheritence (extends one class at a time)
{
	public String name;
	
	public Display2(String nm){
		        name=nm;
	}     
      public void run()			//Change to "run"
      {
		for(int i=0; i<4;i++)
		{ 
			System.out.println(name+": "+i);
			   try{ Thread.sleep(1000);}
		         catch(Exception e){}
		       }
      }
}  


public class runnableExample
{
	public static void main(String[] args)
    {
			Display2 d1=new Display2("First");
			Display2 d2=new Display2("Second");
			Thread t1 = new Thread(d1);						//We must now create the threads here instead
			Thread t2 = new Thread(d2);
			
            t1.start();			//Call the 'start' function which automatically begins the run thread
            t2.start();			//Call the 'start' function which automatically begins the run thread
      }
}
