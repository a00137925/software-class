package q7;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ThreadUnsafe extends Thread {
	static int  val1=9,  val2=2, delay=1000;

	static void Go()
	{
		final Lock lock = new ReentrantLock();
		lock.lock();							//-----------Locks Thread----------------//
		 if  (val2 != 0) System.out.println( val1 / val2);
              val2=0;
		 lock.unlock();							//----------Unlocks Thread---------------//
		}
	
	public void run(){
		    Go();
	}

	
    public static void main( String[] args )
          {         ThreadUnsafe t1=new ThreadUnsafe();
                    ThreadUnsafe t2=new ThreadUnsafe();
				    val1=9; val2=2; 
				    t1.start();
				    delay=1;
				    t2.start();
          }

}
