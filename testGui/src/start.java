import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class start {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JCheckBox chckbxNewCheckBox = new JCheckBox("Greater 10");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					start window = new start();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public start() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 452, 302);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblValue = new JLabel("Value 1");
		lblValue.setBounds(47, 31, 46, 14);
		frame.getContentPane().add(lblValue);
		
		JLabel lblValue_1 = new JLabel("Value 2");
		lblValue_1.setBounds(47, 56, 46, 14);
		frame.getContentPane().add(lblValue_1);
		
		textField = new JTextField();
		textField.setBounds(154, 28, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(154, 53, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int val1 = Integer.parseInt(textField.getText());
				int val2 = Integer.parseInt(textField_1.getText());
				int res = val1 + val2;
				textField_2.setText("" +res);
				
				if(res>10) chckbxNewCheckBox.setSelected(true);
				else chckbxNewCheckBox.setSelected(false);
				
			}
		});
		btnNewButton.setBounds(47, 99, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnSubtract = new JButton("Subtract");
		btnSubtract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				int val1 = Integer.parseInt(textField.getText());
				int val2 = Integer.parseInt(textField_1.getText());
				int res = val1 - val2;
				textField_2.setText("" +res);
				
				if(res>10) chckbxNewCheckBox.setSelected(true);
				else chckbxNewCheckBox.setSelected(false);
			}
		});
		btnSubtract.setBounds(151, 99, 89, 23);
		frame.getContentPane().add(btnSubtract);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(47, 147, 46, 14);
		frame.getContentPane().add(lblResult);
		
		textField_2 = new JTextField();
		textField_2.setBounds(154, 144, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		
		chckbxNewCheckBox.setBounds(83, 200, 97, 23);
		frame.getContentPane().add(chckbxNewCheckBox);
	}
}
