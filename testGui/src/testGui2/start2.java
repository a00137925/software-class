package testGui2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import java.awt.Color;

public class start2 
{

	Counter c = new Counter(1);
	static int check = 0;
	
	static JFrame frame2;
	private JTextField textField;
	JComboBox comboBox = new JComboBox();
	JCheckBox chckbxAlreadyZero = new JCheckBox("Already Zero");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{

						welcome window = new welcome();
						window.frame1.setVisible(true);


				} 
				catch 
				(Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public start2() 
	{
		initialize();
		runCounter();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame2 = new JFrame();
		frame2.setBounds(100, 100, 350, 291);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.getContentPane().setLayout(null);
		
		JLabel lblStepper = new JLabel("Stepper");
		lblStepper.setForeground(new Color(255, 140, 0));
		lblStepper.setBackground(Color.GRAY);
		lblStepper.setHorizontalAlignment(SwingConstants.CENTER);
		lblStepper.setFont(new Font("Viner Hand ITC", Font.PLAIN, 22));
		lblStepper.setBounds(101, 11, 123, 27);
		frame2.getContentPane().add(lblStepper);
		
		JLabel lblValue = new JLabel("Value");
		lblValue.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lblValue.setBounds(101, 64, 46, 14);
		frame2.getContentPane().add(lblValue);
		
		textField = new JTextField();
		textField.setBounds(161, 61, 86, 20);
		frame2.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnIncrement = new JButton("Increment");
		btnIncrement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				c.step();
				runCounter();
				chckbxAlreadyZero.setSelected(false);
			}
		});
		btnIncrement.setBounds(76, 89, 89, 23);
		frame2.getContentPane().add(btnIncrement);
		
		JButton btnDecrement = new JButton("Decrement");
		btnDecrement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if(c.readValue()==0) chckbxAlreadyZero.setSelected(true);
				else c.decr();
				
				runCounter();
			}
		});
		btnDecrement.setBounds(175, 89, 89, 23);
		frame2.getContentPane().add(btnDecrement);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				c.add(comboBox.getSelectedIndex()+1);
				runCounter();
				chckbxAlreadyZero.setSelected(false);
			}
		});
		btnAdd.setBounds(76, 135, 89, 23);
		frame2.getContentPane().add(btnAdd);
		
		
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		comboBox.setBounds(175, 136, 72, 20);
		frame2.getContentPane().add(comboBox);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 57, 21);
		frame2.getContentPane().add(menuBar);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenuItem mntmReset = new JMenuItem("RESET");
		mntmReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				c.reset();
				runCounter();
				
			}
		});
		mnOptions.add(mntmReset);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		mnOptions.add(mntmExit);
		
		
		chckbxAlreadyZero.setBounds(112, 186, 97, 23);
		frame2.getContentPane().add(chckbxAlreadyZero);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frame2.setVisible(false);
			}
		});
		btnClose.setBounds(235, 219, 89, 23);
		frame2.getContentPane().add(btnClose);
	}
	
	public void runCounter()
	{
		textField.setText(""+c.readValue());
	}
}
