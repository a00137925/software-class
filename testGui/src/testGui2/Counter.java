package testGui2;

public class Counter 
{
	private int value;
	
	public Counter(int v) 
	{
		value = v;
		
	}
	
	public int readValue()
	{
		return value;
	}
	
	public void reset()
	{
		value = 0;
	}
	public void step()
	{
		value++;
	}
	
	public void decr()
	{
		value--;
	}
	
	public void add(int v)
	{
		value+=v;
	}

}
