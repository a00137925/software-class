package testGui2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class welcome {

	JFrame frame1;
	static int check = 0;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					welcome window = new welcome();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public welcome() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame1 = new JFrame();
		frame1.setBounds(100, 100, 450, 300);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.getContentPane().setLayout(null);
		
		JLabel lblWelcome = new JLabel("Welcome");
		lblWelcome.setFont(new Font("Georgia", Font.PLAIN, 28));
		lblWelcome.setBounds(141, 69, 137, 33);
		frame1.getContentPane().add(lblWelcome);
		
		JButton btnBegin = new JButton("Begin");
		btnBegin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if(check ==0) 
				{
					start2 start = new start2();
					start.frame2.setVisible(true);	
					check=1;
				}
				else JOptionPane.showMessageDialog(frame1, "Eggs are not supposed to be green.");

			}
		});
		btnBegin.setBounds(151, 150, 89, 23);
		frame1.getContentPane().add(btnBegin);
	}
}
